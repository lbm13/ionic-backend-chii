<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RecurringsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RecurringsTable Test Case
 */
class RecurringsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RecurringsTable
     */
    public $Recurrings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.recurrings',
        'app.surveys',
        'app.questions',
        'app.answer_input_types',
        'app.answer_options',
        'app.answers',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.groups_settings',
        'app.settings_user_types',
        'app.settings_users',
        'app.images',
        'app.videos',
        'app.comments',
        'app.posts',
        'app.from_user',
        'app.followers',
        'app.followees',
        'app.reactions',
        'app.reaction_types',
        'app.posts_reactions',
        'app.reactions_videos',
        'app.to_user',
        'app.comments_posts',
        'app.posts_images',
        'app.comments_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Recurrings') ? [] : ['className' => 'App\Model\Table\RecurringsTable'];
        $this->Recurrings = TableRegistry::get('Recurrings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Recurrings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
