<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AnswerInputTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AnswerInputTypesTable Test Case
 */
class AnswerInputTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AnswerInputTypesTable
     */
    public $AnswerInputTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.answer_input_types',
        'app.questions',
        'app.surveys',
        'app.answer_options',
        'app.answers',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.groups_settings',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.settings_user_types',
        'app.settings_users',
        'app.comments',
        'app.posts',
        'app.from_user',
        'app.followers',
        'app.followees',
        'app.images',
        'app.images_users',
        'app.videos',
        'app.comments_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.reactions',
        'app.posts_reactions',
        'app.reactions_users',
        'app.reactions_videos',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.posts_images',
        'app.to_user',
        'app.comments_posts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AnswerInputTypes') ? [] : ['className' => 'App\Model\Table\AnswerInputTypesTable'];
        $this->AnswerInputTypes = TableRegistry::get('AnswerInputTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AnswerInputTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
