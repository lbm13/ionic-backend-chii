<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserProjectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserProjectsTable Test Case
 */
class UserProjectsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserProjectsTable
     */
    public $UserProjects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_projects',
        'app.projects',
        'app.project_settings',
        'app.settings',
        'app.group_settings',
        'app.groups',
        'app.users',
        'app.message_recipients',
        'app.messages',
        'app.senders',
        'app.posts',
        'app.post_comments',
        'app.post_likes',
        'app.user_settings',
        'app.user_types',
        'app.user_type_settings',
        'app.comments_videos',
        'app.videos',
        'app.video_images',
        'app.video_likes',
        'app.video_tags',
        'app.tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserProjects') ? [] : ['className' => 'App\Model\Table\UserProjectsTable'];
        $this->UserProjects = TableRegistry::get('UserProjects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserProjects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
