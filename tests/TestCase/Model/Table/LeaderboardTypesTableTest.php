<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LeaderboardTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LeaderboardTypesTable Test Case
 */
class LeaderboardTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LeaderboardTypesTable
     */
    public $LeaderboardTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.leaderboard_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LeaderboardTypes') ? [] : ['className' => 'App\Model\Table\LeaderboardTypesTable'];
        $this->LeaderboardTypes = TableRegistry::get('LeaderboardTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LeaderboardTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
