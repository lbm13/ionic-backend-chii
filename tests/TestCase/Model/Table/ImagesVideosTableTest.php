<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ImagesVideosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ImagesVideosTable Test Case
 */
class ImagesVideosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ImagesVideosTable
     */
    public $ImagesVideos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.images_videos',
        'app.videos',
        'app.comments',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.groups_settings',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.settings_user_types',
        'app.settings_users',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.followers',
        'app.images',
        'app.images_users',
        'app.posts',
        'app.from_user',
        'app.reactions',
        'app.posts_reactions',
        'app.reactions_users',
        'app.reactions_videos',
        'app.to_user',
        'app.comments_posts',
        'app.posts_images',
        'app.comments_videos',
        'app.playlists',
        'app.playlists_videos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ImagesVideos') ? [] : ['className' => 'App\Model\Table\ImagesVideosTable'];
        $this->ImagesVideos = TableRegistry::get('ImagesVideos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ImagesVideos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
