<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CommentsPostsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CommentsPostsTable Test Case
 */
class CommentsPostsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CommentsPostsTable
     */
    public $CommentsPosts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.comments_posts',
        'app.posts',
        'app.from_user',
        'app.user_types',
        'app.users',
        'app.groups',
        'app.settings',
        'app.groups_settings',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.settings_user_types',
        'app.settings_users',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.videos',
        'app.comments',
        'app.comments_videos',
        'app.images',
        'app.images_users',
        'app.images_videos',
        'app.posts_images',
        'app.playlists',
        'app.playlists_videos',
        'app.reactions',
        'app.posts_reactions',
        'app.reactions_users',
        'app.reactions_videos',
        'app.tags_videos',
        'app.followers',
        'app.followees',
        'app.to_user'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CommentsPosts') ? [] : ['className' => 'App\Model\Table\CommentsPostsTable'];
        $this->CommentsPosts = TableRegistry::get('CommentsPosts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CommentsPosts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
