<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FitbitActivityTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FitbitActivityTypesTable Test Case
 */
class FitbitActivityTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FitbitActivityTypesTable
     */
    public $FitbitActivityTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fitbit_activity_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FitbitActivityTypes') ? [] : ['className' => 'App\Model\Table\FitbitActivityTypesTable'];
        $this->FitbitActivityTypes = TableRegistry::get('FitbitActivityTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FitbitActivityTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
