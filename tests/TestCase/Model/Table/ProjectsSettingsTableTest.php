<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectsSettingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectsSettingsTable Test Case
 */
class ProjectsSettingsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProjectsSettingsTable
     */
    public $ProjectsSettings;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.projects_settings',
        'app.settings',
        'app.groups',
        'app.users',
        'app.user_types',
        'app.settings_user_types',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.videos',
        'app.comments',
        'app.posts',
        'app.comments_posts',
        'app.images',
        'app.images_users',
        'app.images_videos',
        'app.posts_images',
        'app.reactions',
        'app.posts_reactions',
        'app.reactions_users',
        'app.reactions_videos',
        'app.comments_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.tags_videos',
        'app.followers',
        'app.projects',
        'app.projects_users',
        'app.settings_users',
        'app.groups_settings'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProjectsSettings') ? [] : ['className' => 'App\Model\Table\ProjectsSettingsTable'];
        $this->ProjectsSettings = TableRegistry::get('ProjectsSettings', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProjectsSettings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
