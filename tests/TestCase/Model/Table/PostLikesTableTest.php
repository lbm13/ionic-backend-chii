<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PostLikesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PostLikesTable Test Case
 */
class PostLikesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PostLikesTable
     */
    public $PostLikes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.post_likes',
        'app.posts',
        'app.from_user',
        'app.user_types',
        'app.users',
        'app.groups',
        'app.projects',
        'app.badges',
        'app.settings',
        'app.groups_settings',
        'app.projects_settings',
        'app.settings_user_types',
        'app.settings_users',
        'app.projects_users',
        'app.images',
        'app.videos',
        'app.comments',
        'app.comments_posts',
        'app.comments_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.reactions',
        'app.reactions_videos',
        'app.tags',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.answer_input_types',
        'app.answers',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.posts_images',
        'app.followers',
        'app.followees',
        'app.to_user',
        'app.posts_reactions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PostLikes') ? [] : ['className' => 'App\Model\Table\PostLikesTable'];
        $this->PostLikes = TableRegistry::get('PostLikes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PostLikes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
