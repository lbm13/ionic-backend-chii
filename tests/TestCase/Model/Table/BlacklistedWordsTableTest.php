<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BlacklistedWordsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BlacklistedWordsTable Test Case
 */
class BlacklistedWordsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BlacklistedWordsTable
     */
    public $BlacklistedWords;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.blacklisted_words'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BlacklistedWords') ? [] : ['className' => 'App\Model\Table\BlacklistedWordsTable'];
        $this->BlacklistedWords = TableRegistry::get('BlacklistedWords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BlacklistedWords);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
