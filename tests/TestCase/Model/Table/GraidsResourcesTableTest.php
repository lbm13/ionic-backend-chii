<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GraidsResourcesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GraidsResourcesTable Test Case
 */
class GraidsResourcesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GraidsResourcesTable
     */
    public $GraidsResources;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.graids_resources',
        'app.graids',
        'app.resources'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('GraidsResources') ? [] : ['className' => 'App\Model\Table\GraidsResourcesTable'];
        $this->GraidsResources = TableRegistry::get('GraidsResources', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GraidsResources);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
