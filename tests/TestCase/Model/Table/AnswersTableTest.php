<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AnswersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AnswersTable Test Case
 */
class AnswersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AnswersTable
     */
    public $Answers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.answers',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.fitbit_auth',
        'app.clients',
        'app.badges',
        'app.images',
        'app.notifications',
        'app.playlists',
        'app.videos',
        'app.video_likes',
        'app.comments',
        'app.reported_comments',
        'app.posts',
        'app.from_user',
        'app.fitbit_users',
        'app.fitbit_devices',
        'app.fitbit_activities',
        'app.fitbit_heart_rate_zones',
        'app.followers',
        'app.followees',
        'app.post_likes',
        'app.reported_posts',
        'app.badges_users',
        'app.projects_users',
        'app.settings_users',
        'app.events',
        'app.creators',
        'app.events_users',
        'app.tips',
        'app.answer_options_tips',
        'app.resources',
        'app.graids',
        'app.graid_types',
        'app.graid_numbers',
        'app.graid_constructs',
        'app.chii_questions',
        'app.graids_resources',
        'app.resources_tips',
        'app.tags',
        'app.flags',
        'app.flag_groups',
        'app.flag_groups_flags',
        'app.flag_groups_videos',
        'app.answer_options_tags',
        'app.tags_tips',
        'app.tags_videos',
        'app.tips_users',
        'app.to_user',
        'app.posts_images',
        'app.comments_posts',
        'app.comments_videos',
        'app.images_videos',
        'app.thumbnails',
        'app.playlists_videos',
        'app.badges_projects',
        'app.operations',
        'app.operations_projects',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types',
        'app.answer_input_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Answers') ? [] : ['className' => 'App\Model\Table\AnswersTable'];
        $this->Answers = TableRegistry::get('Answers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Answers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
