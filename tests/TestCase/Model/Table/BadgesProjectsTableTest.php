<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BadgesProjectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BadgesProjectsTable Test Case
 */
class BadgesProjectsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BadgesProjectsTable
     */
    public $BadgesProjects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.badges_projects',
        'app.badges',
        'app.projects',
        'app.groups',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups_settings',
        'app.projects_settings',
        'app.settings_user_types',
        'app.settings_users',
        'app.images',
        'app.videos',
        'app.comments',
        'app.posts',
        'app.from_user',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.followers',
        'app.followees',
        'app.reactions',
        'app.reaction_types',
        'app.posts_reactions',
        'app.reactions_videos',
        'app.projects_users',
        'app.to_user',
        'app.comments_posts',
        'app.posts_images',
        'app.comments_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.badges_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BadgesProjects') ? [] : ['className' => 'App\Model\Table\BadgesProjectsTable'];
        $this->BadgesProjects = TableRegistry::get('BadgesProjects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BadgesProjects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
