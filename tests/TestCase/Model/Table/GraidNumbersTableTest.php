<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GraidNumbersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GraidNumbersTable Test Case
 */
class GraidNumbersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GraidNumbersTable
     */
    public $GraidNumbers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.graid_numbers',
        'app.graids'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('GraidNumbers') ? [] : ['className' => 'App\Model\Table\GraidNumbersTable'];
        $this->GraidNumbers = TableRegistry::get('GraidNumbers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GraidNumbers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
