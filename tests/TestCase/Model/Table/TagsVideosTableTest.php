<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TagsVideosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TagsVideosTable Test Case
 */
class TagsVideosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TagsVideosTable
     */
    public $TagsVideos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tags_videos',
        'app.tags',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answers',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.groups_settings',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.settings_user_types',
        'app.settings_users',
        'app.comments',
        'app.posts',
        'app.comments_posts',
        'app.images',
        'app.images_users',
        'app.videos',
        'app.comments_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.reactions',
        'app.posts_reactions',
        'app.reactions_users',
        'app.reactions_videos',
        'app.posts_images',
        'app.followers',
        'app.answer_options_tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TagsVideos') ? [] : ['className' => 'App\Model\Table\TagsVideosTable'];
        $this->TagsVideos = TableRegistry::get('TagsVideos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TagsVideos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
