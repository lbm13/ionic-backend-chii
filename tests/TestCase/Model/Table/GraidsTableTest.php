<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GraidsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GraidsTable Test Case
 */
class GraidsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GraidsTable
     */
    public $Graids;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.graids',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.fitbit_auth',
        'app.clients',
        'app.badges',
        'app.images',
        'app.videos',
        'app.video_likes',
        'app.comments',
        'app.reported_comments',
        'app.posts',
        'app.from_user',
        'app.events',
        'app.fitbit_users',
        'app.fitbit_devices',
        'app.fitbit_activities',
        'app.fitbit_heart_rate_zones',
        'app.followers',
        'app.followees',
        'app.notifications',
        'app.post_likes',
        'app.reported_posts',
        'app.badges_users',
        'app.projects_users',
        'app.settings_users',
        'app.tips',
        'app.answer_options_tips',
        'app.resources',
        'app.graids_resources',
        'app.resources_tips',
        'app.tags',
        'app.flags',
        'app.flag_groups',
        'app.flag_groups_flags',
        'app.flag_groups_videos',
        'app.answer_options_tags',
        'app.tags_tips',
        'app.tags_videos',
        'app.tips_users',
        'app.to_user',
        'app.posts_images',
        'app.comments_posts',
        'app.comments_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.images_videos',
        'app.badges_projects',
        'app.operations',
        'app.operations_projects',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types',
        'app.answers',
        'app.answer_input_types',
        'app.chii_questions',
        'app.graid_types',
        'app.graid_numbers',
        'app.graid_constructs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Graids') ? [] : ['className' => 'App\Model\Table\GraidsTable'];
        $this->Graids = TableRegistry::get('Graids', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Graids);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
