<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FitbitActivitiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FitbitActivitiesTable Test Case
 */
class FitbitActivitiesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FitbitActivitiesTable
     */
    public $FitbitActivities;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fitbit_activities',
        'app.fitbit_users',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.fitbit_auth',
        'app.clients',
        'app.badges',
        'app.images',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.answers',
        'app.answer_input_types',
        'app.tags',
        'app.answer_options_tags',
        'app.videos',
        'app.video_likes',
        'app.comments',
        'app.reported_comments',
        'app.posts',
        'app.from_user',
        'app.events',
        'app.followers',
        'app.followees',
        'app.notifications',
        'app.post_likes',
        'app.reported_posts',
        'app.badges_users',
        'app.projects_users',
        'app.settings_users',
        'app.to_user',
        'app.posts_images',
        'app.comments_posts',
        'app.comments_videos',
        'app.flag_groups',
        'app.flags',
        'app.flag_groups_flags',
        'app.flag_groups_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.tags_videos',
        'app.badges_projects',
        'app.operations',
        'app.operations_projects',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types',
        'app.fitbit_devices',
        'app.fitbit_heart_rate_zones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FitbitActivities') ? [] : ['className' => 'App\Model\Table\FitbitActivitiesTable'];
        $this->FitbitActivities = TableRegistry::get('FitbitActivities', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FitbitActivities);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
