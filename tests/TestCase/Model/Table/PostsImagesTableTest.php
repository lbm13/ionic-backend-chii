<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PostsImagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PostsImagesTable Test Case
 */
class PostsImagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PostsImagesTable
     */
    public $PostsImages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.posts_images',
        'app.posts',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.groups_settings',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.settings_user_types',
        'app.settings_users',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.videos',
        'app.comments',
        'app.comments_posts',
        'app.comments_videos',
        'app.images',
        'app.images_users',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.reactions',
        'app.posts_reactions',
        'app.reactions_users',
        'app.reactions_videos',
        'app.tags_videos',
        'app.followers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PostsImages') ? [] : ['className' => 'App\Model\Table\PostsImagesTable'];
        $this->PostsImages = TableRegistry::get('PostsImages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PostsImages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
