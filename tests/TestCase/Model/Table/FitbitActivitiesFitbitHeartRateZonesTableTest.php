<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FitbitActivitiesFitbitHeartRateZonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FitbitActivitiesFitbitHeartRateZonesTable Test Case
 */
class FitbitActivitiesFitbitHeartRateZonesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FitbitActivitiesFitbitHeartRateZonesTable
     */
    public $FitbitActivitiesFitbitHeartRateZones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fitbit_activities_fitbit_heart_rate_zones',
        'app.fitbit_activities',
        'app.fitbit_activity_types',
        'app.logs',
        'app.fitbit_heart_rate_zones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FitbitActivitiesFitbitHeartRateZones') ? [] : ['className' => 'App\Model\Table\FitbitActivitiesFitbitHeartRateZonesTable'];
        $this->FitbitActivitiesFitbitHeartRateZones = TableRegistry::get('FitbitActivitiesFitbitHeartRateZones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FitbitActivitiesFitbitHeartRateZones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
