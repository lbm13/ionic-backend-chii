<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConversationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConversationsTable Test Case
 */
class ConversationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConversationsTable
     */
    public $Conversations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.conversations',
        'app.messages',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.badges',
        'app.projects_settings',
        'app.projects_users',
        'app.groups_settings',
        'app.settings_user_types',
        'app.settings_users',
        'app.images',
        'app.videos',
        'app.video_likes',
        'app.comments',
        'app.posts',
        'app.from_user',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.followers',
        'app.followees',
        'app.reactions',
        'app.to_user',
        'app.post_likes',
        'app.comments_posts',
        'app.posts_images',
        'app.posts_reactions',
        'app.comments_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.reactions_videos',
        'app.messages_recipients',
        'app.recipients'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Conversations') ? [] : ['className' => 'App\Model\Table\ConversationsTable'];
        $this->Conversations = TableRegistry::get('Conversations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Conversations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
