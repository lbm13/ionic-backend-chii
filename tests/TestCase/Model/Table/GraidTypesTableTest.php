<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GraidTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GraidTypesTable Test Case
 */
class GraidTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GraidTypesTable
     */
    public $GraidTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.graid_types',
        'app.graids'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('GraidTypes') ? [] : ['className' => 'App\Model\Table\GraidTypesTable'];
        $this->GraidTypes = TableRegistry::get('GraidTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GraidTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
