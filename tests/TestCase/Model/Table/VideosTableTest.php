<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VideosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VideosTable Test Case
 */
class VideosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VideosTable
     */
    public $Videos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.videos',
        'app.video_likes',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.badges',
        'app.images',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.answers',
        'app.answer_input_types',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.posts',
        'app.from_user',
        'app.comments',
        'app.comments_posts',
        'app.comments_videos',
        'app.events',
        'app.fitbit_users',
        'app.fitbit_devices',
        'app.fitbit_activities',
        'app.fitbit_heart_rate_zones',
        'app.followers',
        'app.followees',
        'app.notifications',
        'app.post_likes',
        'app.badges_users',
        'app.projects_users',
        'app.settings_users',
        'app.to_user',
        'app.posts_images',
        'app.images_videos',
        'app.badges_projects',
        'app.operations',
        'app.operations_projects',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types',
        'app.reactions',
        'app.reactions_videos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Videos') ? [] : ['className' => 'App\Model\Table\VideosTable'];
        $this->Videos = TableRegistry::get('Videos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Videos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
