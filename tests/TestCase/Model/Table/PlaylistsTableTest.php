<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PlaylistsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PlaylistsTable Test Case
 */
class PlaylistsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PlaylistsTable
     */
    public $Playlists;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.playlists',
        'app.videos',
        'app.video_likes',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.badges',
        'app.projects_settings',
        'app.projects_users',
        'app.groups_settings',
        'app.settings_user_types',
        'app.settings_users',
        'app.images',
        'app.images_videos',
        'app.posts',
        'app.posts_images',
        'app.post_likes',
        'app.comments',
        'app.comments_posts',
        'app.comments_videos',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.followers',
        'app.followees',
        'app.badges_users',
        'app.playlists_videos',
        'app.reactions',
        'app.reactions_videos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Playlists') ? [] : ['className' => 'App\Model\Table\PlaylistsTable'];
        $this->Playlists = TableRegistry::get('Playlists', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Playlists);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
