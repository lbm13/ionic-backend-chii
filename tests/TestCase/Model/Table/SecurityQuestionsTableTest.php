<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SecurityQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SecurityQuestionsTable Test Case
 */
class SecurityQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SecurityQuestionsTable
     */
    public $SecurityQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.security_questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SecurityQuestions') ? [] : ['className' => 'App\Model\Table\SecurityQuestionsTable'];
        $this->SecurityQuestions = TableRegistry::get('SecurityQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SecurityQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
