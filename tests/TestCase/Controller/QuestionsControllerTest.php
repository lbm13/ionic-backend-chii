<?php
namespace App\Test\TestCase\Controller;

use App\Controller\QuestionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\QuestionsController Test Case
 */
class QuestionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answer_options',
        'app.answers',
        'app.users',
        'app.user_types',
        'app.user_type_settings',
        'app.settings',
        'app.group_settings',
        'app.groups',
        'app.groups_settings',
        'app.project_settings',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.user_settings',
        'app.post_comments',
        'app.posts',
        'app.comments',
        'app.comments_posts',
        'app.videos',
        'app.comments_videos',
        'app.video_images',
        'app.video_likes',
        'app.playlists',
        'app.playlists_videos',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.comments_videos',
        'app.images_videos',
        'app.images',
        'app.images_users',
        'app.posts_images',
        'app.reactions',
        'app.posts_reactions',
        'app.reactions_users',
        'app.post_likes',
        'app.user_projects'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
