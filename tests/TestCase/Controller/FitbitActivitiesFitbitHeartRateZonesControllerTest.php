<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FitbitActivitiesFitbitHeartRateZonesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FitbitActivitiesFitbitHeartRateZonesController Test Case
 */
class FitbitActivitiesFitbitHeartRateZonesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fitbit_activities_fitbit_heart_rate_zones',
        'app.fitbit_activities',
        'app.fitbit_activity_types',
        'app.logs',
        'app.fitbit_heart_rate_zones'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
