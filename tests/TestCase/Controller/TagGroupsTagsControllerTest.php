<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TagGroupsTagsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TagGroupsTagsController Test Case
 */
class TagGroupsTagsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tag_groups_tags',
        'app.tags',
        'app.flags',
        'app.flags_videos',
        'app.videos',
        'app.video_likes',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.fitbit_auth',
        'app.clients',
        'app.badges',
        'app.images',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.answers',
        'app.followees',
        'app.comments',
        'app.reported_comments',
        'app.posts',
        'app.from_user',
        'app.fitbit_users',
        'app.fitbit_devices',
        'app.fitbit_activities',
        'app.fitbit_heart_rate_zones',
        'app.followers',
        'app.notifications',
        'app.post_likes',
        'app.reported_posts',
        'app.badges_users',
        'app.creators',
        'app.projects_users',
        'app.to_user',
        'app.settings_users',
        'app.events',
        'app.events_users',
        'app.tips',
        'app.answer_options_tips',
        'app.resources',
        'app.graids',
        'app.graid_types',
        'app.graid_numbers',
        'app.graid_constructs',
        'app.chii_questions',
        'app.graids_resources',
        'app.resources_tips',
        'app.tags_tips',
        'app.tips_users',
        'app.posts_images',
        'app.comments_posts',
        'app.comments_videos',
        'app.answer_input_types',
        'app.answer_options_tags',
        'app.playlists',
        'app.playlists_videos',
        'app.images_videos',
        'app.badges_projects',
        'app.operations',
        'app.operations_projects',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types',
        'app.flag_groups',
        'app.flag_groups_videos',
        'app.thumbnails',
        'app.tags_videos',
        'app.tag_groups'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
