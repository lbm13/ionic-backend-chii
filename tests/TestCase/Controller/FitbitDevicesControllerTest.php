<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FitbitDevicesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FitbitDevicesController Test Case
 */
class FitbitDevicesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fitbit_devices',
        'app.devices',
        'app.fitbit_users',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.badges',
        'app.badges_users',
        'app.followees',
        'app.images',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.from_user',
        'app.comments',
        'app.posts',
        'app.to_user',
        'app.followers',
        'app.post_likes',
        'app.video_likes',
        'app.videos',
        'app.comments_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.reactions',
        'app.reactions_videos',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.projects_users',
        'app.settings_users',
        'app.posts_images',
        'app.comments_posts',
        'app.answer_input_types',
        'app.answers',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types',
        'app.fitbits'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
