<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SettingsUserTypesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SettingsUserTypesController Test Case
 */
class SettingsUserTypesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.settings_user_types',
        'app.user_types',
        'app.user_type_settings',
        'app.settings',
        'app.groups',
        'app.users',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.videos',
        'app.comments_videos',
        'app.video_images',
        'app.video_likes',
        'app.playlists',
        'app.playlists_videos',
        'app.tags_videos',
        'app.comments_videos',
        'app.comments',
        'app.posts',
        'app.comments_posts',
        'app.images',
        'app.images_users',
        'app.images_videos',
        'app.posts_images',
        'app.reactions',
        'app.posts_reactions',
        'app.reactions_users',
        'app.reactions_videos',
        'app.post_comments',
        'app.post_likes',
        'app.user_projects',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.user_settings',
        'app.settings_users',
        'app.groups_settings'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
