<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FitbitUsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FitbitUsersController Test Case
 */
class FitbitUsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fitbit_users',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.badges',
        'app.images',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.followees',
        'app.comments',
        'app.posts',
        'app.from_user',
        'app.followers',
        'app.post_likes',
        'app.video_likes',
        'app.videos',
        'app.comments_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.reactions',
        'app.reactions_videos',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.badges_users',
        'app.to_user',
        'app.projects_users',
        'app.settings_users',
        'app.posts_images',
        'app.comments_posts',
        'app.answer_input_types',
        'app.answers',
        'app.badges_projects',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types',
        'app.fitbit_devices',
        'app.fitbit_activities',
        'app.fitbit_activity_types',
        'app.fitbit_heart_rate_zones'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
