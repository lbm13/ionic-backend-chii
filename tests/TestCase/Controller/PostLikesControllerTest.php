<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PostLikesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PostLikesController Test Case
 */
class PostLikesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.post_likes',
        'app.posts',
        'app.from_user',
        'app.user_types',
        'app.users',
        'app.groups',
        'app.projects',
        'app.badges',
        'app.settings',
        'app.groups_settings',
        'app.projects_settings',
        'app.settings_user_types',
        'app.settings_users',
        'app.followees',
        'app.images',
        'app.videos',
        'app.comments',
        'app.comments_posts',
        'app.comments_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.reactions',
        'app.reactions_videos',
        'app.posts_reactions',
        'app.tags',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.answer_input_types',
        'app.answers',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.posts_images',
        'app.followers',
        'app.projects_users',
        'app.to_user'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
