<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CommentsPostsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CommentsPostsController Test Case
 */
class CommentsPostsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.comments_posts',
        'app.posts',
        'app.from_user',
        'app.user_types',
        'app.user_type_settings',
        'app.settings',
        'app.group_settings',
        'app.groups',
        'app.users',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.videos',
        'app.comments_videos',
        'app.video_images',
        'app.video_likes',
        'app.playlists',
        'app.playlists_videos',
        'app.tags_videos',
        'app.comments_videos',
        'app.comments',
        'app.post_comments',
        'app.post_likes',
        'app.user_projects',
        'app.projects',
        'app.project_settings',
        'app.user_settings',
        'app.to_user'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
