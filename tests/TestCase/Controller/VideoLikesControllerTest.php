<?php
namespace App\Test\TestCase\Controller;

use App\Controller\VideoLikesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\VideoLikesController Test Case
 */
class VideoLikesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.video_likes',
        'app.videos',
        'app.comments',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.badges',
        'app.projects_settings',
        'app.projects_users',
        'app.followees',
        'app.images',
        'app.images_videos',
        'app.posts',
        'app.from_user',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.followers',
        'app.reactions',
        'app.posts_reactions',
        'app.reactions_videos',
        'app.settings_users',
        'app.to_user',
        'app.comments_posts',
        'app.posts_images',
        'app.groups_settings',
        'app.settings_user_types',
        'app.comments_videos',
        'app.playlists',
        'app.playlists_videos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
