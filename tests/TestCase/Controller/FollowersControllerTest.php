<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FollowersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FollowersController Test Case
 */
class FollowersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.followers',
        'app.follower',
        'app.user_types',
        'app.users',
        'app.groups',
        'app.settings',
        'app.groups_settings',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.from_user',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.videos',
        'app.comments',
        'app.posts',
        'app.to_user',
        'app.images',
        'app.images_users',
        'app.followings',
        'app.reactions',
        'app.posts_reactions',
        'app.reactions_users',
        'app.reactions_videos',
        'app.settings_users',
        'app.images_videos',
        'app.posts_images',
        'app.comments_posts',
        'app.comments_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.tags_videos',
        'app.settings_user_types'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
