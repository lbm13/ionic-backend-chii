<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ChiiQuestionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ChiiQuestionsController Test Case
 */
class ChiiQuestionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.chii_questions',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.fitbit_auth',
        'app.clients',
        'app.badges',
        'app.images',
        'app.answer_options',
        'app.answers',
        'app.tags',
        'app.answer_options_tags',
        'app.videos',
        'app.video_likes',
        'app.comments',
        'app.reported_comments',
        'app.posts',
        'app.from_user',
        'app.events',
        'app.fitbit_users',
        'app.fitbit_devices',
        'app.fitbit_activities',
        'app.fitbit_heart_rate_zones',
        'app.followers',
        'app.followees',
        'app.notifications',
        'app.post_likes',
        'app.reported_posts',
        'app.badges_users',
        'app.to_user',
        'app.projects_users',
        'app.settings_users',
        'app.posts_images',
        'app.comments_posts',
        'app.comments_videos',
        'app.flag_groups',
        'app.flags',
        'app.flag_groups_flags',
        'app.flag_groups_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.tags_videos',
        'app.images_videos',
        'app.badges_projects',
        'app.operations',
        'app.operations_projects',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types',
        'app.answer_input_types'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
