<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FitbitAuthController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FitbitAuthController Test Case
 */
class FitbitAuthControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fitbit_auth',
        'app.clients',
        'app.projects',
        'app.groups',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups_settings',
        'app.projects_settings',
        'app.settings_user_types',
        'app.settings_users',
        'app.followees',
        'app.images',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.answers',
        'app.from_user',
        'app.comments',
        'app.reported_comments',
        'app.posts',
        'app.to_user',
        'app.events',
        'app.fitbit_users',
        'app.fitbit_devices',
        'app.fitbit_activities',
        'app.fitbit_heart_rate_zones',
        'app.followers',
        'app.notifications',
        'app.post_likes',
        'app.reported_posts',
        'app.video_likes',
        'app.videos',
        'app.comments_videos',
        'app.flag_groups',
        'app.flags',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.flag_groups_flags',
        'app.flag_groups_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.badges',
        'app.badges_projects',
        'app.badges_users',
        'app.projects_users',
        'app.posts_images',
        'app.comments_posts',
        'app.answer_input_types',
        'app.operations',
        'app.operations_projects'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
