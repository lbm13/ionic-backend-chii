<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FitbitActivitiesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FitbitActivitiesController Test Case
 */
class FitbitActivitiesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fitbit_activities',
        'app.fitbit_users',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.fitbit_auth',
        'app.clients',
        'app.badges',
        'app.images',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.answers',
        'app.followees',
        'app.comments',
        'app.reported_comments',
        'app.posts',
        'app.from_user',
        'app.events',
        'app.followers',
        'app.notifications',
        'app.post_likes',
        'app.reported_posts',
        'app.video_likes',
        'app.videos',
        'app.comments_videos',
        'app.flag_groups',
        'app.flags',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.flag_groups_flags',
        'app.flag_groups_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.badges_users',
        'app.to_user',
        'app.projects_users',
        'app.settings_users',
        'app.posts_images',
        'app.comments_posts',
        'app.answer_input_types',
        'app.badges_projects',
        'app.operations',
        'app.operations_projects',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types',
        'app.fitbit_devices',
        'app.fitbit_heart_rate_zones'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
