<?php
namespace App\Test\TestCase\Controller;

use App\Controller\VideosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\VideosController Test Case
 */
class VideosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.videos',
        'app.comments',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.groups_settings',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.followees',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.followers',
        'app.reactions',
        'app.reaction_types',
        'app.posts',
        'app.comments_posts',
        'app.images',
        'app.images_users',
        'app.images_videos',
        'app.posts_images',
        'app.posts_reactions',
        'app.posts_reactions_users',
        'app.reactions_users',
        'app.reactions_users_videos',
        'app.reactions_videos',
        'app.settings_users',
        'app.settings_user_types',
        'app.comments_videos',
        'app.playlists',
        'app.playlists_videos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
