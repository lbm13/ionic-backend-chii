<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SurveysController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SurveysController Test Case
 */
class SurveysControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.surveys',
        'app.questions',
        'app.answer_input_types',
        'app.answer_options',
        'app.answers',
        'app.users',
        'app.user_types',
        'app.user_type_settings',
        'app.settings',
        'app.groups',
        'app.groups_settings',
        'app.projects',
        'app.projects_settings',
        'app.projects_users',
        'app.settings_user_types',
        'app.settings_users',
        'app.post_comments',
        'app.posts',
        'app.comments',
        'app.comments_posts',
        'app.videos',
        'app.comments_videos',
        'app.video_images',
        'app.video_likes',
        'app.playlists',
        'app.playlists_videos',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.comments_videos',
        'app.images_videos',
        'app.images',
        'app.images_users',
        'app.posts_images',
        'app.reactions_videos',
        'app.reactions_users',
        'app.reactions',
        'app.posts_reactions',
        'app.post_likes',
        'app.user_projects',
        'app.user_settings'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
