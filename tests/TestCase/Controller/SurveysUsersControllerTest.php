<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SurveysUsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SurveysUsersController Test Case
 */
class SurveysUsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.surveys_users',
        'app.surveys',
        'app.recurrings',
        'app.questions',
        'app.answer_input_types',
        'app.images',
        'app.answer_options',
        'app.answers',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.badges',
        'app.badges_users',
        'app.followees',
        'app.comments',
        'app.posts',
        'app.from_user',
        'app.followers',
        'app.post_likes',
        'app.video_likes',
        'app.videos',
        'app.comments_videos',
        'app.images_videos',
        'app.playlists',
        'app.playlists_videos',
        'app.reactions',
        'app.reactions_videos',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.projects_users',
        'app.to_user',
        'app.settings_users',
        'app.posts_images',
        'app.comments_posts',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
