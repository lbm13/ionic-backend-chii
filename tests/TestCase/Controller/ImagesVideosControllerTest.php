<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ImagesVideosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ImagesVideosController Test Case
 */
class ImagesVideosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.images_videos',
        'app.videos',
        'app.comments_videos',
        'app.users',
        'app.user_types',
        'app.user_type_settings',
        'app.settings',
        'app.group_settings',
        'app.groups',
        'app.groups_settings',
        'app.project_settings',
        'app.projects',
        'app.user_projects',
        'app.user_settings',
        'app.answers',
        'app.questions',
        'app.surveys',
        'app.answer_input_types',
        'app.answer_options',
        'app.tags',
        'app.answer_options_tags',
        'app.tags_videos',
        'app.post_comments',
        'app.posts',
        'app.from_user',
        'app.post_likes',
        'app.video_likes',
        'app.to_user',
        'app.posts_images',
        'app.images',
        'app.images_users',
        'app.video_images',
        'app.playlists',
        'app.playlists_videos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
