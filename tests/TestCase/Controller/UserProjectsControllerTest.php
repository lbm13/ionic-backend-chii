<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UserProjectsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\UserProjectsController Test Case
 */
class UserProjectsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_projects',
        'app.projects',
        'app.project_settings',
        'app.settings',
        'app.group_settings',
        'app.groups',
        'app.users',
        'app.message_recipients',
        'app.messages',
        'app.senders',
        'app.posts',
        'app.post_comments',
        'app.post_likes',
        'app.user_settings',
        'app.user_types',
        'app.user_type_settings',
        'app.comments_videos',
        'app.videos',
        'app.video_images',
        'app.video_likes',
        'app.video_tags',
        'app.tags'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
