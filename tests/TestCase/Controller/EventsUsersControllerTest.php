<?php
namespace App\Test\TestCase\Controller;

use App\Controller\EventsUsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\EventsUsersController Test Case
 */
class EventsUsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.events_users',
        'app.users',
        'app.user_types',
        'app.settings',
        'app.groups',
        'app.projects',
        'app.fitbit_auth',
        'app.clients',
        'app.badges',
        'app.images',
        'app.answer_options',
        'app.questions',
        'app.surveys',
        'app.recurrings',
        'app.surveys_users',
        'app.answers',
        'app.followees',
        'app.comments',
        'app.reported_comments',
        'app.posts',
        'app.from_user',
        'app.events',
        'app.users_for_event',
        'app.fitbit_users',
        'app.fitbit_devices',
        'app.fitbit_activities',
        'app.fitbit_heart_rate_zones',
        'app.followers',
        'app.notifications',
        'app.post_likes',
        'app.reported_posts',
        'app.video_likes',
        'app.videos',
        'app.comments_videos',
        'app.flag_groups',
        'app.flags',
        'app.tags',
        'app.answer_options_tags',
        'app.tips',
        'app.answer_options_tips',
        'app.resources',
        'app.graids',
        'app.graid_types',
        'app.graid_numbers',
        'app.graid_constructs',
        'app.chii_questions',
        'app.graids_resources',
        'app.resources_tips',
        'app.tags_tips',
        'app.tips_users',
        'app.to_user',
        'app.badges_users',
        'app.projects_users',
        'app.settings_users',
        'app.tags_videos',
        'app.flag_groups_flags',
        'app.flag_groups_videos',
        'app.images_videos',
        'app.thumbnails',
        'app.playlists',
        'app.playlists_videos',
        'app.posts_images',
        'app.comments_posts',
        'app.answer_input_types',
        'app.badges_projects',
        'app.operations',
        'app.operations_projects',
        'app.projects_settings',
        'app.groups_settings',
        'app.settings_user_types'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
