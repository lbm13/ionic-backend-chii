<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ResourcesTipsFixture
 *
 */
class ResourcesTipsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'resource_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tip_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_resourcesTips_tipId_tips_idx' => ['type' => 'index', 'columns' => ['tip_id'], 'length' => []],
            'fk_resourcesTips_resourceId_resources_idx' => ['type' => 'index', 'columns' => ['resource_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_resourcesTips_tipId_tips' => ['type' => 'foreign', 'columns' => ['tip_id'], 'references' => ['tips', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_resourcesTips_resourceId_resources' => ['type' => 'foreign', 'columns' => ['resource_id'], 'references' => ['resources', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'created' => '2016-12-07 15:51:36',
            'modified' => '2016-12-07 15:51:36',
            'resource_id' => 1,
            'tip_id' => 1
        ],
    ];
}
