<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GraidsResourcesFixture
 *
 */
class GraidsResourcesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'graid_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'resource_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_graidsResources_graidId_graids_idx' => ['type' => 'index', 'columns' => ['graid_id'], 'length' => []],
            'fk_graidsResources_resourceId_resources_idx' => ['type' => 'index', 'columns' => ['resource_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_graidsResources_graidId_graids' => ['type' => 'foreign', 'columns' => ['graid_id'], 'references' => ['graids', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_graidsResources_resourceId_resources' => ['type' => 'foreign', 'columns' => ['resource_id'], 'references' => ['resources', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'created' => '2016-12-07 15:49:55',
            'modified' => '2016-12-07 15:49:55',
            'graid_id' => 1,
            'resource_id' => 1
        ],
    ];
}
