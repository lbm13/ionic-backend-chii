<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FitbitActivitiesFitbitHeartRateZonesFixture
 *
 */
class FitbitActivitiesFitbitHeartRateZonesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'fitbit_activity_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'fitbit_heart_rate_zone_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fitbit_activity_id' => ['type' => 'index', 'columns' => ['fitbit_activity_id'], 'length' => []],
            'fitbit_heart_rate_zone_id' => ['type' => 'index', 'columns' => ['fitbit_heart_rate_zone_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fitbit_activities_fitbit_heart_rate_zones_ibfk_1' => ['type' => 'foreign', 'columns' => ['fitbit_activity_id'], 'references' => ['fitbit_activities', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fitbit_activities_fitbit_heart_rate_zones_ibfk_2' => ['type' => 'foreign', 'columns' => ['fitbit_heart_rate_zone_id'], 'references' => ['fitbit_heart_rate_zones', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'fitbit_activity_id' => 1,
            'fitbit_heart_rate_zone_id' => 1,
            'created' => '2016-11-15 15:30:27',
            'modified' => '2016-11-15 15:30:27'
        ],
    ];
}
