<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GraidsFixture
 *
 */
class GraidsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'graid_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'graid_number_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'text' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'graid_construct_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_graids_graidTypeId_graidTypes_idx' => ['type' => 'index', 'columns' => ['graid_type_id'], 'length' => []],
            'fk_graids_graidNumberId_graidNumbers_idx' => ['type' => 'index', 'columns' => ['graid_number_id'], 'length' => []],
            'fk_graids_graidConstructId_graidConstructs_idx' => ['type' => 'index', 'columns' => ['graid_construct_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_graids_graidConstructId_graidConstructs' => ['type' => 'foreign', 'columns' => ['graid_construct_id'], 'references' => ['graid_constructs', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_graids_graidNumberId_graidNumbers' => ['type' => 'foreign', 'columns' => ['graid_number_id'], 'references' => ['graid_numbers', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_graids_graidTypeId_graidTypes' => ['type' => 'foreign', 'columns' => ['graid_type_id'], 'references' => ['graid_types', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'created' => '2016-12-12 17:07:24',
            'modified' => '2016-12-12 17:07:24',
            'graid_type_id' => 1,
            'graid_number_id' => 1,
            'text' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'graid_construct_id' => 1
        ],
    ];
}
