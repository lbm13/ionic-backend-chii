<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AnswerOptionsTipsFixture
 *
 */
class AnswerOptionsTipsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'tip_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'answer_option_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_answerOptionsTips_tipId_tips_idx' => ['type' => 'index', 'columns' => ['tip_id'], 'length' => []],
            'fk_answerOptionsTips_answerOptionId_answerOptions_idx' => ['type' => 'index', 'columns' => ['answer_option_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_answerOptionsTips_tipId_tips' => ['type' => 'foreign', 'columns' => ['tip_id'], 'references' => ['tips', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_answerOptionsTips_answerOptionId_answerOptions' => ['type' => 'foreign', 'columns' => ['answer_option_id'], 'references' => ['answer_options', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'created' => '2016-12-07 15:52:16',
            'modified' => '2016-12-07 15:52:16',
            'tip_id' => 1,
            'answer_option_id' => 1
        ],
    ];
}
