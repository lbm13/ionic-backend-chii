<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AnswersFixture
 *
 */
class AnswersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'response' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'answer_option_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'surveys_users_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'question_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_answers_answerOptionId_answerOptions_idx' => ['type' => 'index', 'columns' => ['answer_option_id'], 'length' => []],
            'fk_answers_questionId_questions_idx' => ['type' => 'index', 'columns' => ['question_id'], 'length' => []],
            'fk_answers_surveysUsersId_surveysUsers_idx' => ['type' => 'index', 'columns' => ['surveys_users_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_answers_surveysUsersId_surveysUsers' => ['type' => 'foreign', 'columns' => ['surveys_users_id'], 'references' => ['surveys_users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'fk_answers_answerOptionId_answerOptions' => ['type' => 'foreign', 'columns' => ['answer_option_id'], 'references' => ['answer_options', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
            'fk_answers_questionId_questions' => ['type' => 'foreign', 'columns' => ['question_id'], 'references' => ['questions', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'response' => 'Lorem ipsum dolor sit amet',
            'created' => '2017-02-06 05:41:46',
            'modified' => '2017-02-06 05:41:46',
            'answer_option_id' => 1,
            'surveys_users_id' => 1,
            'question_id' => 1
        ],
    ];
}
