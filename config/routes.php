<?php
use Cake\Routing\Router;

Router::defaultRouteClass('Route');
Router::extensions('json');

Router::scope(
    '/videos',
    ['controller' => 'Videos'],
    function ($routes) {
        $routes->connect('/get/page', ['action' => 'page']);
    }
);

Router::scope('/', function($routes) {
    $routes->fallbacks('InflectedRoute');
});