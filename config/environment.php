<?php
return [
    /**
     * Connection information used by the ORM to connect
     * to your application's datastores.
     * Drivers include Mysql Postgres Sqlite Sqlserver
     * See vendor\cakephp\cakephp\src\Database\Driver for complete list
     */
    'Datasources' => [
        /** automation */
//        'default' => [
//            'className' => 'Cake\Database\Connection',
//            'driver' => 'Cake\Database\Driver\Mysql',
//            'persistent' => false,
//            'host' => 'powersid.uic-chp.org',
//            'port' => '3306',
//            'username' => 'automation',
//            'password' => 'myC09g*5',
//            'database' => 'automation',
//            'encoding' => 'utf8',
//            'timezone' => 'UTC',
//            'flags' => [],
//            'cacheMetadata' => true,
//            'log' => false,
//            'quoteIdentifiers' => false,
//            'url' => env('DATABASE_URL', null)
//        ],
    /** Local */
//        'default' => [
//            'className' => 'Cake\Database\Connection',
//            'driver' => 'Cake\Database\Driver\Mysql',
//            'persistent' => false,
//            'host' => 'localhost',
//            'port' => '8888',
//            'username' => 'root',
//            'password' => '',
//            'database' => 'ionic-demo',
//            'encoding' => 'utf8',
//            'timezone' => 'UTC',
//            'flags' => [],
//            'cacheMetadata' => true,
//            'log' => false,
//            'quoteIdentifiers' => false,
//            'url' => env('DATABASE_URL', null)
//        ],
    /** Testing **/
//        'default' => [
//            'className' => 'Cake\Database\Connection',
//            'driver' => 'Cake\Database\Driver\Mysql',
//            'persistent' => false,
//            'host' => 'powersid.uic-chp.org',
//            'port' => '3306',
//            'username' => 'testing',
//            'password' => '2Bxm1s_7',
//            'database' => 'testing',
//            'encoding' => 'utf8',
//            'timezone' => 'UTC',
//            'flags' => [],
//            'cacheMetadata' => true,
//            'log' => false,
//            'quoteIdentifiers' => false,
//            'url' => env('DATABASE_URL', null)
//        ],
    /** Development **/
        'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Mysql',
            'persistent' => false,
//            'host' => 'powersid.uic-chp.org',
//            'port' => '3306',
//            'username' => 'ionic',
//            'password' => '0yBtm@55',
//            'database' => 'ionic-demo',
            'host' => 'localhost',
            'port' => '8888',
            'username' => 'root',
            'password' => '',
            'database' => 'ionic-demo',
            'encoding' => 'utf8',
            'timezone' => 'UTC',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => false,
            'url' => env('DATABASE_URL', null)
        ],
    ]
];
