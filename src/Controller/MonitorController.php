<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * MessagesRecipients Controller
 *
 * @property \App\Model\Table\MessagesRecipientsTable $MessagesRecipients
 */
class MonitorController extends AppController {
    const VALID_TYPES = ['comments', 'posts'];

    public function report($type = null, $id = null) {
        $id = $this->getData('id', $id);
        $type = $this->getData('type', $type);

        if (is_null($id)) {
            $this->response->addError('ID is required.');
        } else if (is_null($type)) {
            $this->response->addError('Type is required.');
        } else {
            $type = strtolower($type);
            $model = TableRegistry::get('reported_'.$type);
            if ($model) {
                $columnName = Inflector::singularize($type);

                $newEntity = $model->newEntity([
                    $columnName.'_id' => $id,
                    'user_id'         => $GLOBALS['user']->id
                ]);
                $newEntity = $model->save($newEntity);

                if ($newEntity) {
                    $event = new Event('monitor.add.'.$type, $this, $newEntity);
                    $this->eventManager()->dispatch($event);
                } else {
                    $this->response->addError('Could not create report.');
                }
            } else {
                $this->response->addError('Unknown type of \''.$type.'\'. Valid types are: '.implode(', ', self::VALID_TYPES));
            }
        }
    }
}
