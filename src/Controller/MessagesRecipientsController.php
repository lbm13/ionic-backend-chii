<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * MessagesRecipients Controller
 *
 * @property \App\Model\Table\MessagesRecipientsTable $MessagesRecipients
 */
class MessagesRecipientsController extends AppController {

    public function unreadCount($id = null) {
        if ($id == null) {
            $id = $GLOBALS['user']->id;
        }
        $messages = TableRegistry::get('MessagesRecipients')->find('all')->where(['recipient_id' => $id, 'has_read' => 0]);
        $this->response->addMessage('count', $messages->count());
    }

}
