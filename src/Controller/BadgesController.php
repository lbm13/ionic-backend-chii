<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Service\BadgeService;

/**
 * Badges Controller
 *
 * @property \App\Model\Table\BadgesTable $Badges
 */
class BadgesController extends AppController
{
    public function getBadges($userId = null, $selectedProjectId = null) {
        if (!is_null($selectedProjectId) && $selectedProjectId > 0) {
            $badgeService = new BadgeService();
            $badges = $badgeService->getBadges($userId, $selectedProjectId);
            $this->response->addMessage('badges', $badges);
        } else {
            parent::get();
        }
    }
}
