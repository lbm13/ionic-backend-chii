<?php
namespace App\Controller;

use App\Network\HttpResponseCode;
use App\Service\SurveyService;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Event\Event;


/**
 * Surveys Controller
 *
 * @property \App\Model\Table\SurveysTable $Surveys
 */
class SurveysController extends AppController {

    public function add() {
        $survey = parent::add();
        $this->handleEvents($survey);
    }

    public function get($id = null) {
        $surveys = [];
        $results = parent::get($id);

        $userId = $this->getData('SurveysUsers.user_id');
        if (is_null($userId)) {
            $userId = $GLOBALS['user']->id;
        }

        if (count($results) == 0 && $userId) {
            $prefix = 'SurveysUsers';
            foreach ($_GET as $key => $value) {
                if (strpos($key, $prefix) === 0) {
                    unset($_GET[$key]);
                }
            }

            $results = parent::get($id);
        }

        foreach ($results as $survey) {
            if (is_array($survey->surveys_users)) {
                if (count($survey->surveys_users) > 0) {
                    $surveysUsers = [];
                    foreach ($survey->surveys_users as $surveyUser) {
                        if (!is_null($userId) && $surveyUser->user_id == $userId) {
                            if (count($surveyUser->answers) === 0) {
                                $answers = new \stdClass();
                            } else {
                                $answers = [];
                                foreach ($surveyUser->answers as $answer) {
                                    $answers[$answer->question_id] = $answer;
                                }
                            }
                            $surveyUser->answers = $answers;

                            $surveysUsers[$surveyUser->response_order] = $surveyUser;
                        } else {
                            unset($surveyUser);
                        }
                    }

                    $survey->surveys_users = $surveysUsers;
                } else {
                    $survey->surveys_users = ['0' => new \stdClass()];
                    $survey->surveys_users['0']->answers = new \stdClass();
                    $survey->surveys_users['0']->is_locked = false;
                    $survey->surveys_users['0']->last_resent = null;
                    $survey->surveys_users['0']->response_order = 0;
                    $survey->surveys_users['0']->survey_id = $survey->id;
                    $survey->surveys_users['0']->user_id = $GLOBALS['user']->id;
                }
            }

            $surveys[$survey->id] = $survey;
        }

        $this->response->clearMessages();
        $this->response->addMessage($this->loadModel()->table(), $surveys);
    }

    public function lock() {
        $this->toggleLock(true);
    }

    public function resend($surveyId = null) {
        $surveyId = $this->getData('survey_id', $surveyId);
        $userId = $this->getData('user_id');

        $surveyService = new SurveyService();
        $surveyService->resend($surveyId, $userId);

        $this->response->addMessage('message', 'Success');
    }

    /**
     * Save a survey's answers
     */
    public function save() {
        if (array_key_exists('survey', $_POST)) {
            $this->saveSurvey($_POST['survey']);
        }
    }

    private function saveSurvey($survey) {
        if (array_key_exists('surveys_users', $survey)) {
            $answerModel = TableRegistry::get('Answers');
            $surveyUsers = $survey['surveys_users'];
            foreach ($surveyUsers as $response => $surveyUser) {
                $answers = $surveyUser['answers'];

                $surveyUserModel = TableRegistry::get('SurveysUsers');
                if (array_key_exists('id', $surveyUser)) {
                    $ogSurveyUser = $surveyUserModel->find('all')->where(
                        [
                            'id' => $surveyUser['id'],
                            'survey_id' => $survey['id'],
                            'user_id' => $GLOBALS['user']->id
                        ]
                    )->first();

                    if (!array_key_exists('last_resent', $surveyUser) || is_null($surveyUser['last_resent'])) {
                        $surveyUser['last_resent'] = date("Y-m-d H:i:s");
                    }

                    $surveyUser = $surveyUserModel->patchEntity($ogSurveyUser, $surveyUser);
                } else {
                    if (array_key_exists('is_locked', $surveyUser)) {
                        $isLocked = $surveyUser['is_locked'];
                    } else {
                        $isLocked = ($response == 0) ? 0 : 1;
                    }

                    if (array_key_exists('last_resent', $surveyUser)) {
                        $lastResent = $surveyUser['last_resent'];
                    } else {
                        $lastResent = date("Y-m-d H:i:s");
                    }

                    $surveyUser = $surveyUserModel->newEntity([
                        'is_locked'      => $isLocked,
                        'last_resent'    => $lastResent,
                        'response_order' => $response,
                        'survey_id'      => $survey['id'],
                        'user_id'        => $GLOBALS['user']->id
                    ]);
                }

                if (count($surveyUser->errors()) === 0) {
                    $surveyUser = $surveyUserModel->save($surveyUser);

                    if (!$surveyUser) {
                        $this->response->addError('Could not update surveys_users');
                    } else {
                        foreach ($answers as $questionId => $answer) {
                            $answer['surveys_users_id'] = $surveyUser->id;
                            $answer['question_id'] = $questionId;
                            $answer = $answerModel->findOrCreate($answer);
                            if ($answer) {
                                $this->response->addMessage('answers', $answer);
                            } else {
                                $this->response->addError('Could not save the answer');
                            }
                        }
                    }
                } else {
                    $this->response->addErrors($surveyUser->errors());
                }
            }
        }
    }

    public function synchronize() {
        if (array_key_exists('surveys', $_POST)) {
            foreach ($_POST['surveys'] as $survey) {
                $this->saveSurvey($survey);
            }
        }

        $this->get();
    }

    private function toggleLock($value) {
        $surveyId = $this->getData('survey_id');
        $userId = $this->getData('user_id');

        if (is_null($surveyId)) {
            $this->response->addError('Must include survey_id.');
        } else if (is_null($userId)) {
            $this->response->addError('Must include user_id.');
        } else {
            $surveyService = new SurveyService();
            $surveyService->toggleLock($surveyId, $userId, $value);

            $this->response->addMessage('messages', 'Success.');
        }
    }

    public function unlock() {
        $this->toggleLock(false);
    }

    public function updatePositions() {
        $what = $_POST['what'];
        unset($_POST[0]);
        $inputs = $_POST['inputs'];
        $theModel = TableRegistry::get($what);

        foreach($inputs as $input) {
            $field = $theModel->get($input['id']);
            $field->position = $input['position'];
            $theModel->save($field);
        }
    }

    public function handleEvents($survey) {
        if ($survey) {
            if (!is_null($survey->project_id)) {
                $projectsUsersModel = TableRegistry::get('ProjectsUsers');
                $usersProjects = $projectsUsersModel->find('all')->select('id')->where(['project_id' => $survey->project_id])->hydrate(false)->toList();
                $due = $survey->due;
                if (is_null($due)) {
                    $due = Time::now();
                    $due = $due->addWeek(1);
                }
                $data = new \stdClass();
                $data->title = 'Take Survey '.$survey->name;
                $data->description = 'Please take this survey by this date';
                $data->users = $usersProjects;

                $data->start = $due;
                $data->end = $due;
                $this->generateEvent($data, 'survey.add.after');
                $this->generateEvent($data, 'survey.add.notify');
            }
        }
    }

    public function generateEvent($data, $key) {
        $event = new Event($key, $this, $data);
        $this->eventManager()->dispatch($event);
    }
}
