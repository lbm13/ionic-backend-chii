<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Network\HttpResponseCode;
use App\Service\AuthService;
use App\Service\MonitorService;
use App\Service\NotificationService;
use App\Service\EventService;
use Cake\Controller\Controller;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\Association;
use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Utility\Inflector;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        $this->autoRender = false;

        if (in_array($_SERVER['REQUEST_METHOD'], ['POST', 'OPTIONS']) && empty($_POST)) {
            $_POST = json_decode(file_get_contents('php://input'), true);

            if (is_null($_POST)) {
                $_POST = [];
            }
        }

        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');

        $this->eventManager()->on(new NotificationService());
        $this->eventManager()->on(new MonitorService());
        $this->eventManager()->on(new EventService());
    }

    /**
     * Before controller action
     *
     * @param Event $event
     *
     * @return void
     */
    public function beforeFilter(Event $event) {
        if ($this->request->params['action'] == 'fitbitAuthorization' || $this->request->params['action'] == 'retrieveAnalytics') {
            return;
        }

        $this->validateToken();
    }

    /**
     * Before render callback.
     *
     * @param Event $event
     *
     * @return void
     */
    public function beforeRender(Event $event) {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Insert model into database from POST
     *
     * @param []|null $array
     *
     * @return EntityInterface|boolean
     */
    public function add() {
        if (count($_POST) > 0) {
            $model = $this->loadModel();
            $params = $this->removeModelFields($model, $_POST);

            $newEntity = $model->newEntity($params);
            $object = $model->save($newEntity);
            if ($object) {
                foreach ($_POST as $key => $value) {
                    /** @var Association\BelongsToMany|Association\BelongsToMany $association */
                    $association = $this->getAssociation($key);
                    if (!is_null($association)) {
                        if (!is_array($value)) {
                            $value = [$value];
                        }
                        $associatedModel = $this->loadModel($association->table());
                        switch (get_class($association)) {
                            case BelongsToMany::class:
                                $associatedObjects = [];
                                foreach ($value as $associatedObject) {
                                    $associatedObject = $this->removeModelFields($associatedModel, $associatedObject);
                                    $associatedObjects[] = $associatedModel->findOrCreate($associatedObject);
                                }
                                $association->link($object, $associatedObjects);
                                break;
                        }
                    }
                }

                $this->response->addMessage($model->table(), $object, HttpResponseCode::RESPONSE_CREATED);

                return $object;
            } else {
                $this->response->addError('The '.$model->table().' could not be created');
                if ($newEntity->errors()) {
                    $this->response->addErrors($newEntity->errors());
                }
            }
        } else {
            $this->response->addError('This method only accepts POST data.');
        }

        return false;
    }

    /**
     * @param Query   $query
     * @param mixed[] $joins
     *
     * @return Query
     */
    private function addJoins($query, $joins) {
        foreach ($joins as $key => $value) {
            if ($value == '') {
                $query->contain([$key]);
            } else {
                $join = substr($key, 0, strrpos($key, '.'));
                $key = implode('.', array_slice(explode('.', $key), -2, 2, true));

                $query->contain([$join]);
                $query->innerJoinWith(
                    $join,
                    function($q) use ($key, $value) {
                        return $q->where([$key => $value]);
                    }
                );
            }
        }

        return $query;
    }

    /**
     * @param Query $query
     *
     * @return Query
     */
    private function addLimitAndOffset($query) {
        $limit = $this->getLimit();
        $offset = $this->getOffset();
        if (!is_null($limit)) {
            $query->limit($limit);
            if (!is_null($offset)) {
                $query->offset($offset);
            }
        }

        return $query;
    }

    /**
     * @param Query $query
     *
     * @return Query
     */
    private function addOrder($query) {
        $order = $this->getOrder();
        if (!is_null($order)) {
            $query->order([$order]);
        }

        return $query;
    }

    /**
     * @param Query   $query
     * @param mixed[] $params
     *
     * @return Query
     */
    private function addWhere($query, $params) {
        foreach ($params as $equalsKey => $equalsValue) {
            $query->andWhere([$equalsKey => $equalsValue]);
        }

        return $query;
    }

    /**
     * @param Query   $query
     * @param mixed[] $where
     *
     * @return Query
     */
    private function addWhereOr($query, $where) {
        foreach ($where as $orKey => $orValue) {
            $query->orWhere([$orKey => $orValue]);
        }

        return $query;
    }

    /**
     * Get all posted associations and either find or create them
     *
     * @return array
     */
    private function buildAssociations() {
        $associations = [];
        foreach ($_POST as $key => $objects) {
            if (count($objects) > 0) {
                $association = $this->getAssociation($key);

                if ($association) {
                    $associationTable = $association->target();

                    if (!is_array($objects) || get_class($association) === BelongsTo::class) {
                        $objects = [$objects];
                    }

                    foreach ($objects as $object) {
                        if (is_array($object)) {
                            if (array_key_exists('id', $object)) {
                                $associationObject = $associationTable->find('all')->where(['id' => $object['id']])->first();
                            } else {
                                $associationObject = $associationTable->save($associationTable->newEntity($object));
                            }

                            if ($associationObject) {
                                if (!array_key_exists($association->name(), $associations)) {
                                    $associations[$association->name()] = [];
                                }

                                $associations[$association->name()][] = $associationObject;

                            } else {
                                $this->response->addError('Could not find or create ' . json_encode($object));
                            }
                        }
                    }
                }
            }
        }

        return $associations;
    }

    /**
     * Check if array contains object
     *
     * @param $array
     * @param $object
     *
     * @return bool
     */
    private function containsObject($array, $object) {
        foreach ($array as $item) {
            $itemId = (is_array($item)) ? $item['id'] : $item->id;
            $objectId = (is_array($object)) ? $object['id'] : $object->id;
            if ($itemId === $objectId) {
                return true;
            }
        }

        return false;
    }

    /**
     * Delete an object from the database by id
     *
     * @param null $id
     */
    public function delete($id = null) {
        $model = $this->loadModel();

        if (!is_null($id)) {
            try {
                $object = $model->get($id, []);

                if ($model->delete($object)) {
                    $this->response->addMessage('messages', 'Successfully deleted the object', HttpResponseCode::RESPONSE_DELETED);
                } else {
                    $this->response->addError('Could not delete the object', HttpResponseCode::RESPONSE_INTERNAL_SERVER_ERROR);
                }
            } catch (RecordNotFoundException $e) {
                $this->response->addError('Object cannot be found with id '.$id, HttpResponseCode::RESPONSE_NOT_FOUND);
            }
        } else {
            $params = $this->getParameters()['equals'];

            /** @var Query $query */
            $query = $model->find('all');
            $query->where($params);
            if ($query->count() == 1) {
                $object = $query->toArray()[0];

                if ($model->delete($object)) {
                    $this->response->addMessage('messages', 'Successfully deleted the object', HttpResponseCode::RESPONSE_DELETED);
                } else {
                    $this->response->addError('Could not delete the object', HttpResponseCode::RESPONSE_INTERNAL_SERVER_ERROR);
                }
            } else {
                $this->response->addError('Too vague to delete '.$model->table());
            }
        }
    }

    /**
     * Edit an entity by id
     *
     * @param null $id
     */
    public function edit($id = null) {
        if ($this->request->is('post')) {
            $id = $this->getData('id', $id);
            if (!is_null($id)) {
                /** @var Table $model */
                $model = $this->loadModel();

                $associations = $this->buildAssociations();

                $object = $model->patchEntity($model->get($id, ['contain' => array_keys($associations)]), $_POST);
                $object = $this->unlinkAllAssociations($object, $associations);
                $object = $this->linkAssociations($object, $associations);

                $this->saveObject($object);
            } else {
                $this->response->addError('Id is required to edit.');
            }
        } else {
            $this->response->addError('To edit a model you can only use POST', HttpResponseCode::RESPONSE_NOT_ALLOWED);
        }
    }

    public function get($id = null) {
        $id = $this->getData('id', $id);
        if (!is_null($id) && (string) (int) $id == $id) {
            $_POST['id'] = $id;
        }

        $params = $this->getParameters();
        $model = $this->loadModel();
        $query = $model->find('all', ['fields' => $this->getFields()]);
        $query = $this->addWhere($query, $params['equals']);
        $query = $this->addWhereOr($query, $params['or']);
        $query = $this->addJoins($query, $params['joins']);
        $query = $this->addOrder($query);
        $query = $this->addLimitAndOffset($query);
        $query = $query->distinct();

        $results = [];
        if (!is_null($this->getData('page'))) {
            try {
                $results = $this->Paginator->paginate($query, $this->paginate);
            } catch(NotFoundException $e) {
                $this->response->addError('No more records.', HttpResponseCode::RESPONSE_NOT_FOUND);
            }
        } else {
            $results = $query->toArray();
        }

        $this->response->addMessage($model->table(), $results);

        return $results;
    }

    /**
     * Tries finding the model's related association using name
     *
     * @param string $name
     * @param null   $model
     *
     * @return Association|null
     */
    private function getAssociation($name, $model = null) {
        if (is_null($model)) {
            $model = $this->loadModel();
        }
        $association = $model->associations()->get($name);
        if (!$association) {
            $key = str_replace('_', '', $name);
            $key = Inflector::pluralize($key);
            $association = $model->associations()->get($key);
        }

        return $association;
    }

    /**
     * Check GET and POST for the field value
     *
     * @param string|string[] $fields
     * @param mixed|null      $value
     *
     * @return null
     */
    protected function getData($fields, $value = null) {
        if (!is_array($fields)) {
            $fields = [$fields];
        }

        if (!is_null($value)) {
            return $value;
        }

        foreach ($fields as $field) {
            $field = str_replace('.', '_', $field);
            if (array_key_exists($field, $_GET)) {
                return $_GET[$field];
            } else if (array_key_exists($field, $_POST)) {
                return $_POST[$field];
            }
        }

        return null;
    }

    /**
     * Get the fields meant to be selected
     *
     * @return array
     */
    private function getFields() {
        $fields = [];
        if (array_key_exists('fields', $_GET)) {
            $fields = explode(',', $_GET['fields']);
        }

        return $fields;
    }

    /**
     * Get the query limit
     *
     * @return null
     */
    private function getLimit() {
        $limit = null;
        if (array_key_exists('limit', $_GET)) {
            $limit = $_GET['limit'];
        }

        return $limit;
    }

    /**
     * Get the query offset
     *
     * @return null
     */
    private function getOffset() {
        $offset = null;
        if (array_key_exists('offset', $_GET)) {
            $offset = $_GET['limit'];
        }

        return $offset;
    }

    /**
     * Get the query's order by value
     *
     * @return null|string
     */
    private function getOrder() {
        $results = [];

        if (array_key_exists('order', $_GET)) {
            $orders = explode(',', $_GET['order']);

            foreach ($orders as $order) {
                $sign = substr($order, 0, 1);
                $direction = 'ASC';
                if (in_array($sign, ['+', '-'])) {
                    if ($sign === '-') {
                        $direction = 'DESC';
                    }

                    $order = substr($order, 1);
                }

                $result = $order;
                if (strpos($result, '.') === false) {
                    $result = $this->loadModel()->alias() . '.' . $order;
                }
                $result = $result . ' ' . $direction;

                $results[] = $result;
            }
        }

        return (count($results) == 0) ? null : implode(',', $results);
    }

    /**
     * Get all parameters, including associations
     *
     * @return mixed[]
     */
    public function getParameters() {
        $params = [
            'equals' => [],
            'joins'  => [],
            'or'     => []
        ];

        $array = array_merge($_GET, $_POST);
        foreach ($array as $field => $value) {
            $comparisonType = 'equals';
            $operator = substr($field, strlen($field) - 1);
            switch($operator) {
                case '!':
                case '>':
                case '<':
                    $field = substr($field, 0, strlen($field) - 1);
                    $operator .= '=';
                    break;
                case '|':
                    $field = substr($field, 0, strlen($field) - 1);
                    $comparisonType = 'or';
                    $operator = '=';
                    break;
                case '%':
                    $field = substr($field, 0, strlen($field) - 1);
                    $operator = 'LIKE';
                    $value = '%'.$value.'%';
                    break;
                default:
                    $operator = '=';
            }

            if (!is_array($value)) {
                if (strtolower($value) == 'null') {
                    $operator = (strpos($operator, '!') === false) ? 'IS' : 'IS NOT';
                    $value = null;
                } else if (preg_match('/^([0-9]{1,},{1})([0-9]{1,},*)*$/', $value)) {
                    $operator = 'IN';
                    $value = explode(',', $value);
                }
            }

            $key = '';
            $model = $this->loadModel();
            do {
                if ($key != '') {
                    $field = substr($field, strpos($field, '_') + 1);
                }

                $alias = (strpos($field, '_') !== false) ? substr($field, 0, strpos($field, '_')) : $field;
                $association = $model->associations()->get($alias);
                if (is_null($association)) {
                    break;
                } else {
                    $model = $this->loadModel($association->target()->table());

                    if (is_null($model)) {
                        break;
                    } else {
                        if ($key != '') {
                            $key .= '.';
                        }
                        $key .= $alias;

                        if ($model->hasField($field)) {
                            $params[$comparisonType][$key . $field . ' ' . $operator] = $value;
                            break;
                        }
                    }
                }
            } while (strpos($field, '_') !== false);

            if ($key != '') {
                if ($value != '') {
                    $key .= '.'.$field;
                    if ($operator != '=') {
                        $key .= ' '.$operator;
                    }
                }
                $params['joins'][$key] = $value;
            } else {
                if ($model->hasField($field)) {
                    $field = $model->alias().'.'.$field;
                    if ($operator != '=') {
                        $field .= ' '.$operator;
                    }
                    $params[$comparisonType][$field] = $value;
                }
            }
        }

        return $params;
    }

    /**
     * Check if the logged in user is of type []
     * @param $allowedUserTypes
     * @param $respond
     *
     * @return bool
     */
    public function is($allowedUserTypes, $respond = false) {
        if (!is_array($allowedUserTypes)) {
            $allowedUserTypes = [$allowedUserTypes];
        }

        $returnVar = in_array($GLOBALS['user_type']->name, $allowedUserTypes);

        if (!$returnVar && $respond) {
            $this->response->addError('Action not allowed by '.$GLOBALS['user_type']->name);
            $this->response->kill();
        }

        return $returnVar;
    }

    /**
     * Checks if the logged in user is an admin
     *
     * @param $respond
     *
     * @return bool
     */
    public function isAdmin($respond = false) {
        return $this->is(['admin'], $respond);
    }

    /**
     * Check if the logged in user's user type is above a minimum
     *
     * @param $minimumUserType
     * @param bool $respond
     *
     * @return bool
     */
    public function isAtLeast($minimumUserType, $respond = false) {
        $userTypes = [
            'admin'              => 1,
            'projectCoordinator' => 2,
            'physician'          => 3,
            'participant'        => 4,
            'buddy'              => 5
        ];

        $returnVar = ($userTypes[$GLOBALS['user_type']->name] <= $userTypes[$minimumUserType]);

        if (!$returnVar && $respond) {
            $this->response->addError('Action not allowed by '.$GLOBALS['user_type']->name.'. Must be at least type \''.$minimumUserType.'\' to complete the action.');
            $this->response->kill();
        }

        return $returnVar;
    }

    /**
     * Add associations to the model (found by id)
     *
     * @param null $id
     */
    public function link($id = null) {
        $id = $this->getData('id', $id);
        $associations = $this->buildAssociations();

        $model = $this->loadModel();
        $object = $model->get($id, ['contain' => array_keys($associations)]);
        if ($object) {
            $object = $this->linkAssociations($object, $associations);
            $this->saveObject($object);
        } else {
            $this->response->addError('Cannot find object with id '.$id);
        }
    }

    /**
     * Find/create associations and then link to the model
     *
     * @param Entity   $object
     * @param string[] $associations
     *
     * @return mixed
     */
    private function linkAssociations($object, $associations) {
        if (count($associations) > 0) {
            foreach ($associations as $association => $associationObjects) {
                /** @var BelongsTo|BelongsToMany $association */
                $association = $this->getAssociation($association);
                if ($association) {
                    if (!is_array($associationObjects)) {
                        $associationObjects = [$associationObjects];
                    }

                    $newAssociationObjects = [];
                    foreach ($associationObjects as $associationObject) {
                        if (!$this->containsObject($object[$association->property()], $associationObject)) {
                            $newAssociationObjects[] = $associationObject;
                        }
                    }

                    switch(get_class($association)) {
                        case BelongsTo::class:
                            if ($object->source() === $association->source()->alias()) {
                                $object[$association->foreignKey()] = $newAssociationObjects[0]['id'];
                            }
                            break;
                        case BelongsToMany::class:
                            $association->link($object, $newAssociationObjects);
                            break;
                    }
                }
            }
        }

        return $object;
    }

    /**
     * Call for pagination
     */
    public function page() {
        $model = $this->loadModel();
        $this->response->addMessage($model->table(), $this->paginate());
    }

    /**
     * Find all fields on the model and remove the others from the array
     *
     * @param Table $model
     * @param $array
     *
     * @return array
     */
    public function removeModelFields($model, $array) {
        $params = [];
        foreach ($array as $key => $value) {
            if ($model->hasField($key)) {
                $params[$key] = $value;
                unset($array[$key]);
            }
        }

        return $params;
    }

    /**
     * Save an object with associations
     *
     * @param EntityInterface $object
     */
    private function saveObject($object) {
        $model = $this->loadModel();
        if ($model->save($object, ['associated' => true])) {
            $this->response->addMessage($model->table(), $object->jsonSerialize());
        } else {
            $this->response->addError('Could not edit '.$model->table().'.');
            $this->response->addErrors($object->errors());
        }
    }

    /**
     * Remove given associations from the object (found by id)
     *
     * @param null $id
     */
    public function unlink($id = null) {
        $id = $this->getData('id', $id);
        $associations = $this->buildAssociations();

        $model = $this->loadModel();
        $object = $model->get($id, ['contain' => array_keys($associations)]);
        if ($object) {
            $object = $this->unlinkAssociations($object, $associations);
            $this->saveObject($object);
        } else {
            $this->response->addError('Cannot find object with id '.$id);
        }
    }

    /**
     * Remove all associations unless object is added to exceptions
     *
     * @param $object
     * @param $exceptions
     *
     * @return bool|\Cake\Datasource\EntityInterface|mixed
     */
    private function unlinkAllAssociations($object, $exceptions) {
        $remove = [];
        foreach (array_keys($exceptions) as $associationName) {
            $association = $this->getAssociation($associationName);
            foreach ($object[$association->property()] as $associationObject) {
                if (!$this->containsObject($exceptions[$association->alias()], $associationObject)) {
                    if (!array_key_exists($associationName, $remove)) {
                        $remove[$associationName] = [];
                    }

                    $remove[$associationName][] = $associationObject;
                }
            }
        }

        return (count($remove) > 0) ? $this->unlinkAssociations($object, $remove) : $object;
    }

    /**
     * Delete any passed in associations
     *
     * @param Entity $object
     * @param string[] $associations
     *
     * @return bool|\Cake\Datasource\EntityInterface|mixed
     */
    private function unlinkAssociations($object, $associations) {
        foreach ($associations as $association => $associationObjects) {
            /** @var BelongsTo|BelongsToMany $association */
            $association = $this->getAssociation($association);

            if ($association) {
                switch(get_class($association)) {
                    case BelongsTo::class:
                        if ($object->source() === $association->source()->alias()) {
                            $object[$association->foreignKey()] = null;
                        }
                        break;
                    case BelongsToMany::class:
                        $association->unlink($object, $associationObjects);
                        $object = $association->save($object);
                }
            }
        }

        return $object;
    }

    /**
     * Add the object using the logged in user's user_id
     *
     * @param string $key
     *
     * @return boolean
     */
    protected function usingToken($key='user_id') {
        if (!array_key_exists($key, $_POST)) {
            $_POST[$key] = $GLOBALS['user']->id;
        }

        if ($GLOBALS['user']->id == $_POST[$key]) {
            return true;
        } else {
            $this->response->addError('You can only perform this action on '.$this->loadModel()->table().' from your own account.', HttpResponseCode::RESPONSE_NOT_ALLOWED);
            $this->response->kill();
        }

        return false;
    }

    /**
     * Checks if the token is authentic
     */
    private function validateToken() {
        $token = $this->getData('token');

        if (is_null($token)) {
            $this->response->addError("No token");
            $this->response->kill();
        }

        $auth = new AuthService();
        $user = $auth->checkToken($token);
        if ($user) {
            $GLOBALS['user'] = $user;
            $GLOBALS['user_type'] = $user->getUserType();
        } else {
            $this->response->addError('Invalid token');
            $this->response->kill();
        }
    }
}
