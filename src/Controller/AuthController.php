<?php
namespace App\Controller;

use App\Network\HttpResponseCode;
use App\Model\Entity\User;
use App\Network\UABResponse;
use App\Service\AuthService;
use App\Service\UserService;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Auth Controller
 *
 * Controller for handling authentication
 *
 * @property UABResponse $this->response
 */
class AuthController extends AppController {
    /** @var AuthService */
    protected $service;

    /**
     * Do nothing, extending would mean a token is required for this controller
     *
     * @param Event $event
     *
     * @return void
     */
    public function beforeFilter(Event $event) {}

    /**
     * Initialize the controller
     */
    public function initialize() {
        $this->service = new AuthService();

        parent::initialize();
    }

    /**
     * Check if a user's token is authorized from GET|POST
     */
    public function checkAuth() {
        $token = $this->getData('token');
        if (is_null($token)) {
            $this->response->addError('Token missing from request');
        } else {
            /** @var User $user */
            $user = $this->service->checkToken($token);
            if (!$user) {
                $this->response->addError('Token is not valid', HttpResponseCode::RESPONSE_NOT_ALLOWED);
            } else {
                $this->response->addMessage('user', $user);
                $this->response->addMessage('token', $token);
            }
        }
    }

    /**
     * Generate a token for the user to use API
     */
    public function login() {
        if (count($_POST) > 0) {
            $query = TableRegistry::get('Users')->find();
            $query->contain(['Images', 'Groups', 'Projects', 'UserTypes']);
            $query->where([
                "username" => $_POST["username"]
            ]);

            /** @var User $user */
            $user = $query->last();
            if ($user && $this->service->compare($_POST['password'], $user->password, false)) {
                $this->response->addMessage('token', $this->service->generateToken($user));
                $this->response->addMessage('user', $user);
            } else {
                $this->response->addError('Incorrect username or password');
            }
        } else {
            $this->response->addError('Login requires POST');
        }
    }

    /**
     * Expire a user's token
     */
    public function logout() {
        $token = false;
        if (array_key_exists("token", $_POST)) {
            $token = $_POST["token"];
        } else if (array_key_exists("token", $_GET)) {
            $token = $_GET["token"];
        } else {
            $this->response->addError('Not valid request type');
        }

        if ($token) {
            $value = $this->service->expireToken($token);

            if ($value) {
                $this->response->addMessage('messages', 'Successfully logged out');
            } else {
                $this->response->addError('Token is not valid');
            }
        } else {
            $this->response->addError('Token missing from request');
        }
    }

    /**
     * Reset user's password if security answer matches
     *
     * @param null $username
     */
    public function reset($username = null) {
        $securityAnswer = null;
        if (array_key_exists('security_answer', $_POST)) {
            $securityAnswer = $_POST['security_answer'];
        } else if (array_key_exists('security_answer', $_GET)) {
            $securityAnswer = $_GET['security_answer'];
        }

        if (is_null($username)) {
            $this->response->addError("Username is not defined.", HttpResponseCode::RESPONSE_INVALID_FIELD);
        } else {
            $userController = new UsersController();
            $query = $userController->Users->find();
            $query->where([
                'username'       => $username,
                'security_answer' => $securityAnswer
            ]);
            /** @var User $user */
            $user = $query->last();

            if ($user) {
                $password = null;
                if (array_key_exists('password', $_POST)) {
                    $password = $_POST['password'];
                } else if (array_key_exists('password', $_GET)) {
                    $password = $_GET['password'];
                }

                if (is_null($password) || $password === 'null') {
                    $this->response->addError('You must include the new password for the user.', HttpResponseCode::RESPONSE_NOT_CHANGED);
                } else {
                    $user->password = $password;
                    $userController->Users->save($user);

                    $this->response->addMessage('message', 'Password reset.');
                }
            } else {
                $this->response->addError("Security answer does not match for the given username", HttpResponseCode::RESPONSE_NOT_ALLOWED);
            }
        }
    }

    /**
     * Returns the security question associated with the submitted user
     *
     * @param null $username
     */
    public function security($username = null) {
        if (is_null($username)) {
            $this->response->addError("Username is not defined.", HttpResponseCode::RESPONSE_INVALID_FIELD);
        } else {
            $userController = new UsersController();
            $query = $userController->Users->find();
            $query->where([
                "username" => $username
            ]);
            $user = $query->last();

            if ($user) {
                $security = [
                    "id"                => $user->id,
                    "security_question" => $user->security_question
                ];

                $this->response->addMessage('user', $security);
            } else {
                $this->response->addError("Username not found: ".$username, HttpResponseCode::RESPONSE_NOT_FOUND);
            }
        }
    }
}
