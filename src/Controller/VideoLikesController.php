<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * VideoLikes Controller
 *
 * @property \App\Model\Table\VideoLikesTable $VideoLikes
 */
class VideoLikesController extends AppController{
    public function add($id = null) {
        if (parent::usingToken()) {
            parent::add();
        }
    }

    public function delete($id = null) {
        if (parent::usingToken()) {
            parent::delete();
        }
    }
}
