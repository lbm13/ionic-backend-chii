<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Network\HttpResponseCode;
use App\Service\SurveyService;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Event\Event;

/**
 * SurveysUsers Controller
 *
 * @property \App\Model\Table\SurveysUsersTable $SurveysUsers
 */
class SurveysUsersController extends AppController {
    public function add() {
        $surveyUser = parent::add();
        $this->handleEvents($surveyUser);
    }

    public function handleEvents($surveyUser) {
        if ($surveyUser) {
            if (!is_null($surveyUser->user_id)) {
                $data = new \stdClass();
                $data->user_id = $surveyUser->user_id;
                $surveysModel = TableRegistry::get('Surveys');
                $survey = $surveysModel->get($surveyUser->survey_id);
                $due = $survey->due;
                if (is_null($due)) {
                    $due = Time::now();
                    $due = $due->addWeek(1);
                }
                $data->title = 'Take Survey '.$survey->name;
                $data->description = 'Please take this survey by this date';
                $data->start = $due;
                $data->end = $due;

                $this->generateEvent($data, 'user.add.after');
                $this->generateEvent($data, 'event.add.after');
            }
        }
    }

    public function generateEvent($data, $key) {
        $event = new Event($key, $this, $data);
        $this->eventManager()->dispatch($event);
    }
}
