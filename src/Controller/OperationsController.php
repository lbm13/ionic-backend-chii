<?php
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Operations Controller
 *
 * @property \App\Model\Table\OperationsTable $Operations
 */
class OperationsController extends AppController {
    public function get($id = null) {
        $operations = parent::get($id);

        $project = false;
        if (count($operations) == 0) {
            $projectId = $this->getData('OperationsProjects.project_id');
            if (!is_null($projectId)) {
                $project = TableRegistry::get('Projects')->find('all')->where(['id' => $projectId])->first();

                unset($_GET['OperationsProjects_project_id']);
                unset($_POST['OperationsProjects_project_id']);
            }

            if ($project) {
                $operations = parent::get($id);
            }
        }

        $this->response->clearMessages();

        foreach ($operations as $operation) {
            if ($operation->operations_projects) {
                if (is_array($operation->operations_projects) && count($operation->operations_projects) == 1) {
                    $operation->operations_projects = $operation->operations_projects[0];
                } else if ($project && count($operation->operations_projects) == 0) {
                    $operation->operations_projects = $this->getOperationsProjects($operation->id, $project->id);
                }
            } else {
                if ($project) {
                    $operation->operations_projects = $this->getOperationsProjects($operation->id, $project->id);
                }
            }
        }

        $this->response->addMessage($this->loadModel()->table(), $operations);
    }

    private function getOperationsProjects($operationId, $projectId) {
        $operationProject = new \stdClass();
        $operationProject->operation_id = $operationId;
        $operationProject->project_id = $projectId;
        $operationProject->enabled = false;

        return $operationProject;
    }

    public function edit($id = null) {
        $id = $this->getData('id', $id);
        if (is_null($id)) {
            $operationModel = TableRegistry::get('Operations');
            $operationProjectModel = TableRegistry::get('OperationsProjects');

            $operations = $this->getData('operations');
            if (!is_null($operations)) {
                foreach ($operations as $key => $operation) {
                    $operationProject = $operationProjectModel->findOrCreate($operation['operations_projects']);

                    if ($operationProject) {
                        $operations[$key]['operations_projects'] = $operationProject;
                    } else {
                        $this->response->addError('Could not update operation.');
                    }
                }
            } else {
                $operations = [];
            }

            $this->response->addMessage($operationModel->table(), $operations);
        } else {
            parent::edit($id);
        }
    }

    public function synchronize($projectId = null) {
        $this->edit();

        $projectId = $this->getData('project_id', $projectId);
        if (is_null($projectId)) {
            $projectId = $GLOBALS['user']->projects[0]->id;
        }

        $_GET['OperationsProjects_project_id'] = $projectId;

        $this->get();
    }
}
