<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Network\HttpResponseCode;
use Cake\Event\Event;

/**
 * SettingsUsers Controller
 *
 * @property \App\Model\Table\SettingsUsersTable $SettingsUsers
 */
class SettingsUsersController extends AppController {
    public function beforeFilter(Event $event) {
        $this->response->addError('You must use the settings controller.', HttpResponseCode::RESPONSE_NOT_ALLOWED);
        $this->response->kill();
    }
}
