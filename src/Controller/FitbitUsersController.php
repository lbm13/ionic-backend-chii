<?php
namespace App\Controller;

use App\Network\HttpResponseCode;
use App\Service\FitBitService;
use Cake\ORM\TableRegistry;

/**
 * Fitbit Users class that handles fitbit authorization and accessing fitbit data with help of the fitbit service.
 *
 * Class FitbitUsersController
 * @package App\Controller
 */
class FitbitUsersController extends AppController {
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function fitbitAuthorization() {
        $code = $this->getData('code');
        if (is_null($code)) {
            $this->response->addErrors('Code is required.');
        }

        $state = $this->getData('state');
        if (is_null($state)) {
            $this->response->addError('State is required.');
        }

        if (!$this->response->hasErrors()) {
            $state = explode(',', $state);
            if (count($state) == 4) {
                $userId = $state[0];
                $projectId = $state[1];
                $redirectUri = $state[2];
                $redirect = $state[3];

                $fitbitService = new FitbitService();
                if ($fitbitService->authorize($userId, $projectId, $code, $redirectUri)) {
                    $this->redirect($redirect);
                } else {
                    $this->response->addError('Unable to update FitbitUser.');
                }
            } else {
                $this->response->addError('Invalid state.');
            }
        }
    }

    public function forceUpdate($projectId = null, $userId = null, $afterDate = null) {
        $fitbitService = new FitbitService();
        $activities = $fitbitService->updateActivities($projectId, $userId, $afterDate);
        if ($activities) {
            $this->response->addMessage('activities', $activities);
        } else {
            $this->response->addError('Could not retrieve the user\'s activities');
        }
    }
}
