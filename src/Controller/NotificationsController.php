<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Network\HttpResponseCode;

/**
 * Notifications Controller
 *
 * @property \App\Model\Table\NotificationsTable $Notifications
 */
class NotificationsController extends AppController {
    public $paginate = [
        'limit' => 10,
        'order' => [
            'has_read' => 'asc',
            'created' => 'desc'
        ]
    ];

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function get($id = null) {
        if (parent::usingToken()) {
            parent::get($id);
        }
    }

    public function unreadCount($id = null) {
        if ($id == null) {
            $id = $GLOBALS['user']->id;
        }
        $notifications = TableRegistry::get('Notifications')->find('all')->where(['user_id' => $id, 'has_read' => 0]);
        $this->response->addMessage('count', $notifications->count());
    }
}