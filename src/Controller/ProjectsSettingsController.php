<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Network\HttpResponseCode;
use Cake\Event\Event;
use Cake\Network\Response;

/**
 * ProjectsSettings Controller
 *
 * @property \App\Model\Table\ProjectsSettingsTable $ProjectsSettings
 */
class ProjectsSettingsController extends AppController {
    public function beforeFilter(Event $event) {
        $this->response->addError('You must use the settings controller.', HttpResponseCode::RESPONSE_NOT_ALLOWED);
        $this->response->kill();
    }
}
