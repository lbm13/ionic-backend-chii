<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;

class UploadController extends AppController
{
    const UPLOAD_DIRECTORY_IMG = '/img/';
    const UPLOAD_DIRECTORY_VIDEO = '/videos/';
    const MAX_FILE_SIZE = '5242880'; //5 MB

    //Keeping upload function so as to not break uabUpload
    public function upload() {
        if (count($_FILES) > 0) {
            foreach ($_FILES as $file) {
                $fileName = explode(".", $file['name']);
                $fileName = time() . '_' . rand(100, 999) . '.' . end($fileName);
                $filePath = $_SERVER['DOCUMENT_ROOT'] . self::UPLOAD_DIRECTORY . $fileName;

                if (!file_exists($filePath)) {
                    if (move_uploaded_file($file["tmp_name"], $filePath)) {
                        $imagesTable = TableRegistry::get('Images');
                        $image = $imagesTable->save(
                            $imagesTable->newEntity(
                                [
                                    'file_path' => $filePath
                                ]
                            )
                        );

                        if ($image) {
                            $this->response->addMessage('image', $image);
                            return $image;
                        } else {
                            $this->response->addError('Could not create image object for file: ' . $file['name']);
                        }
                    } else {
                        $this->response->addError('Can not move file: ' . $file['name']);
                    }
                } else {
                    $this->response->addError('File already exists: ' . $file['name']);
                }
            }
        } else {
            $this->response->addError('No file included for upload');
        }
    }

    public function uploadImage($title) {
        if (count($_FILES) > 0) {
            $file = $_FILES['file'];
            if ($file['size'] < self::MAX_FILE_SIZE * 2) {
                if ($file['size'] > self::MAX_FILE_SIZE) {
                    $file = $this->massageFileSize($file);
                }
                $ext = explode("/", $file['type']);
                $fileName = time() . '_' . rand(100, 999) . '.' . end($ext);
                $filePath = $this->getFilePath($fileName, self::UPLOAD_DIRECTORY_IMG);
                if (!file_exists($filePath)) {
                    if (move_uploaded_file($file["tmp_name"], $filePath)) {
                        $imagesTable = TableRegistry::get('Images');
                        $image = $imagesTable->save(
                            $imagesTable->newEntity(
                                [
                                    'file_path' => 'img/' . $fileName,
                                    'title' => $title
                                ]
                            )
                        );

                        if ($image) {
                            $this->response->addMessage('image', $image);
                        } else {
                            $this->response->addError('Could not create image object for file: ' . $file['name']);
                        }
                    } else {
                        $this->response->addError('Can not move file: ' . $file['name']);
                    }
                } else {
                    $this->response->addError('File already exists: ' . $file['name']);
                }
            } else {
                $this->response->addError('File is too large');
            }
        } else {
            $this->response->addError('No file included for upload');
        }
    }

    public function uploadVideo() {
        if (count($_FILES) > 0) {
            $file = $_FILES['file'];
            $ext = explode("/", $file['type']);
            $fileName = time() . '_' . rand(100, 999) . '.' . end($ext);
            $filePath = $this->getFilePath($fileName, self::UPLOAD_DIRECTORY_VIDEO);
            if (!file_exists($filePath)) {
                if (move_uploaded_file($file['tmp_name'], $filePath)) {
                    $this->response->addMessage('videoPath', $this->getUploadDir(self::UPLOAD_DIRECTORY_VIDEO) . $fileName);
                } else {
                    $this->response->addError('Can not move file: ' . $file['name']);
                }
            } else {
                $this->response->addError('File already exists: ' . $file['name']);
            }
        } else {
            $this->response->addError('No file included for upload');
        }
    }

    public function massageImageSize($file) {
        $this->log('was i called?');
        $percentage = 90;
        $filesize = getimagesize($file['tmp_name']);
        $width = $filesize[0];
        $height = $filesize[1];
        $tn = imagecreatetruecolor($width, $height) ;
        $image = imagecreatefromjpeg($file['tmp_name']) ;
        imagecopyresampled($tn, $image, 0, 0, 0, 0, $width, $height, $width, $height);
        imagejpeg($tn, $file['tmp_name'], $percentage);
        return $file;
    }

    public function getFilePath($fileName, $uploadDir) {
        $uploadDir = $this->getUploadDir($uploadDir);
        return $_SERVER['DOCUMENT_ROOT'] . $uploadDir . $fileName;
    }

    public function getUploadDir($uploadDir) {
        $origin = $_SERVER['HTTP_ORIGIN'] . '';
        $referer = $_SERVER['HTTP_REFERER'] . '';
        return '/' . explode('/', str_replace($origin, "", $referer))[1] . $uploadDir;
    }

    public function uploadProfileImage($userID) {
        $image = $this->upload();
        if ($image) {
            $usersTable = TableRegistry::get('Users');
            $user = $usersTable->get($userID);
            $user->image_id = $image->id;
            $usersTable->save($user);
        } else {
            $this->response->addError('Failed to upload profile picture');
        }

    }
}
