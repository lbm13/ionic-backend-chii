<?

namespace App\Controller\Component;

use Cake\Controller\Component;

class UploadComponent extends Component
{
//    const BASE_URL = '/opt/lampp/htdocs';
//    const UPLOAD_DIRECTORY = '/webroot/img/uploaded';
    const UPLOAD_DIRECTORY = '/ImageUpload/images/';
    
    public function move($file) {
        $baseUrl = $_SERVER['DOCUMENT_ROOT'];
        $this->autoRender = false;
        $fileName = $file['name'];
        $temp = explode(".", $fileName);
        $newfilename = substr(md5($file['tmp_name']), 0, 10) . '.' . end($temp);
        $filePath = $baseUrl . self::UPLOAD_DIRECTORY . $newfilename;
        move_uploaded_file($file['tmp_name'], $filePath);
        return self::UPLOAD_DIRECTORY . $newfilename;
    }
    
    public function delete($path) {
        $baseUrl = $_SERVER['DOCUMENT_ROOT'];
        $path = $baseUrl . $path;
        unlink($path);
    }
    
    public function swap($index1, $index2) {
        $temp = $index1;
        $index1 = $index2;
        $index2 = $temp;
    }
}