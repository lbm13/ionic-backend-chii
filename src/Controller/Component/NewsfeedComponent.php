<?

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class NewsfeedComponent extends Component {
    public function autoGenerate($message, $imageId = null) {
        $newPost = [
            'from_user_id' => $GLOBALS['user']->id,
            'to_user_id'   => $GLOBALS['user']->id,
            'message'      => $message
        ];
        if (!is_null($imageId)) {
            $newPost['image_id'] = $imageId;
        }

        $model = TableRegistry::get('Posts');
        $model->findOrCreate($newPost);
    }
}
