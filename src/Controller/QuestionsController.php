<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Network\HttpResponseCode;

/**
 * Questions Controller
 *
 * @property \App\Model\Table\QuestionsTable $Questions
 */
class QuestionsController extends AppController
{
    public function add() {
        $questionModel = $this->loadModel('Questions');
        $surveyId = $this->getData('survey_id');
        $position = 1;
        $questionWithHighestPosition = $questionModel->find('all', [
            'order' => ['position' => 'desc'],
            'conditions' => ['survey_id' => $surveyId]
        ])->first();
        if ($questionWithHighestPosition != null) $position = $questionWithHighestPosition->position + 1;
        $_POST['position'] = $position;
        if (array_key_exists('image', $_POST)) {
            unset($_POST['image']);
        }
        $addedQuestion = parent::add();
        if (array_key_exists('chii_questions', $_POST)) {
            $chii_question = $_POST['chii_questions'];
            $chii_questions = TableRegistry::get('ChiiQuestions');
            $new_question = $chii_questions->newEntity();
            $new_question->question_id = $addedQuestion->id;
            $new_question->chii_number = $chii_question['chii_number'];
            if (isset($chii_question['graid_construct_id'])) {
                $new_question->graid_construct_id = $chii_question['graid_construct_id'];
            }
            $object = $chii_questions->save($new_question);
            $addedQuestion->chii_questions = [$object];
        }
        $this->response->clearMessages();
        $this->response->addMessage('questions', $addedQuestion, HttpResponseCode::RESPONSE_CREATED);
    }
}
