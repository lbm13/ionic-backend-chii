<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CommentsPosts Controller
 *
 * @property \App\Model\Table\CommentsPostsTable $CommentsPosts
 */
class CommentsPostsController extends AppController {
    public function add() {
        if (parent::usingToken()) {
            parent::add();
        }
    }
}
