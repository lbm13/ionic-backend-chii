<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Network\HttpResponseCode;

/**
 * Playlists Controller
 *
 * @property \App\Model\Table\PlaylistsTable $Playlists
 */
class PlaylistsController extends AppController {
    public function initialize() {
        parent::initialize();
    }

    public function synchronize() {
        $this->get();
    }

    public function add() {
        $object = parent::add();
        if ($object->image_id) {
            $object['image'] = TableRegistry::get('Images')->get($object->image_id);
            $this->response->clearMessages();
            $this->response->addMessage('playlists', $object, HttpResponseCode::RESPONSE_CREATED);
        }
    }
}
