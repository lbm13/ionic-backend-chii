<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Response;

/**
 * Events Controller
 *
 * @property \App\Model\Table\EventsTable $Events
 */
class EventsController extends AppController {
    public function add() {
        if (parent::usingToken()) {
            $event = new Event('event.add.after', $this, parent::add());
            $this->eventManager()->dispatch($event);
        }
    }

    public function synchronize() {
        parent::get();
    }
}
