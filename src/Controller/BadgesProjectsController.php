<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Network\HttpResponseCode;

/**
 * BadgesProjects Controller
 *
 * @property \App\Model\Table\BadgesProjectsTable $BadgesProjects
 */
class BadgesProjectsController extends AppController {
    public function add() {
        $object = parent::add();
        if ($object->badge_id) {
            $object['badge'] = TableRegistry::get('Badges')->get($object->badge_id, ['contain' => ['Images']]);
            $this->response->clearMessages();
            $this->response->addMessage('badges_projects', $object, HttpResponseCode::RESPONSE_CREATED);
        }
    }
}
