<?php
namespace App\Controller;

class VideosController extends AppController {
    public $paginate = [
        'limit' => 20,
        'order' => [
            'Videos.title' => 'asc'
        ]
    ];

    public function initialize() {
        parent::initialize();
    }

    public function synchronize() {
        $this->get();
    }

    public function add() {
        if (isset($_POST['thumbnail'])) {
            unset($_POST['thumbnail']);
        }
        parent::add();
    }

    public function edit($id = null) {
        if (!is_null($_POST['thumbnail'])) {
            unset($_POST['thumbnail']);
        }
        if (!is_null($_POST['flags'])) {
            unset($_POST['flags']);
        }

        parent::edit();
    }
}
