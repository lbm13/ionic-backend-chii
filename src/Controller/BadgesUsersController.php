<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\BadgesUser;
use Cake\ORM\TableRegistry;

/**
 * BadgesUsers Controller
 *
 * @property \App\Model\Table\BadgesUsersTable $BadgesUsers
 */
class BadgesUsersController extends AppController
{
    public function initialize() {
        $this->loadComponent('Newsfeed');
    }

    public function add() {
        /** @var BadgesUser $badgesUsers */
        $badgesUsers = parent::add();
        $badge = TableRegistry::get('Badges')->find('all', ['id' => $badgesUsers->badge_id]);

        $this->Newsfeed->autoGenerate('You\'ve earned a new badge!', $badge->image_id);
    }
}
