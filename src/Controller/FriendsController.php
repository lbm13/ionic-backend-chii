<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Response;
use App\Network\HttpResponseCode;
use App\Service\FollowerService;
use App\Service\UserService;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Friends Controller
 */
class FriendsController extends AppController {
    public function add($id = null) {
        $followerService = new FollowerService();
        $object = $followerService->follow($GLOBALS['user']->id, $this->getData('following_id', $id));
        $this->response->addMessage('friends', $object);

        $userService = new UserService();
        if ($userService->isFollowing($GLOBALS['user'], $userService->getById($id))) {
            $event = new Event('friend.add.after', $this, $object);
            $this->eventManager()->dispatch($event);
        } else {
            $event = new Event('friend.request.after', $this, $object);
            $this->eventManager()->dispatch($event);
        }
    }

    public function get($id = null) {
        if (parent::usingToken()) {
            $userId = $_POST['user_id'];

            $query = TableRegistry::get('Users')->find('all', []);
            $query->join([
                [
                    'table' => 'followers',
                    'alias' => 'e',
                    'conditions' => [
                        'Users.id = e.user_id',
                        'e.following_id = '.$userId
                    ]
                ],
                [
                    'table' => 'followers',
                    'alias' => 'i',
                    'conditions' => [
                        'Users.id = i.following_id'
                    ]
                ]
            ]);

            $this->response->addMessage('friends', $query->toArray());
        }
    }

    public function remove($id = null) {
        $followerService = new FollowerService();
        if (
            $followerService->unfollow($GLOBALS['user']->id, $this->getData('following_id', $id))
            && $followerService->unfollow($this->getData('following_id', $id), $GLOBALS['user']->id)
        ) {
            $this->response->addMessage('messages', 'Successfully deleted.', HttpResponseCode::RESPONSE_DELETED);
        } else {
            $this->response->addError('Could not remove the friendship.', HttpResponseCode::RESPONSE_INTERNAL_SERVER_ERROR);
        }
    }
}
