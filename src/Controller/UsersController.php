<?php
namespace App\Controller;

use App\Model\Entity\Response;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

class UsersController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function add() {
        if (array_key_exists('image', $_POST)) {
            unset($_POST['image']);
        }
        $user = parent::add();
//        $this->handleEvents($user);
    }

    public function edit($id = null) {
        if (array_key_exists('image', $_POST)) {
            unset($_POST['image']);
        }
        parent::edit($id);
    }

    public function handleEvents($user) {
        if ($user) {
            $projectsModel = TableRegistry::get('ProjectsUsers');
            $surveysModel = TableRegistry::get('Surveys');
            $usersProjects = $projectsModel->find('list', ['keyField' => 'project_id', 'valueField' => 'project_id'])->where(['user_id' => $user->id])->toArray();
            $surveys = $surveysModel->find('all')->where(['project_id IN' => $usersProjects])->toArray();
            foreach($surveys as $survey) {
                $due = $survey->due;
                $recurringId = 0;
                if (!is_null($survey->recurring_id)) {
                    $recurringId = $survey->recurring_id;
                }
                $data = new \stdClass();
                $data->title = 'Take Survey '.$survey->name;
                $data->description = 'Please take this survey by this date';
                $data->userId = $user->id;

                if ($recurringId == 0) {
                    if ($due) {
                        $data->start = $due;
                        $data->end = $due;
                        $this->generateEvent($data);
                    }
                } else {
                    $this->handleRecurrings($recurringId, $survey, $due, $data);
                }
            }
        }
    }

    public function handleRecurrings($recurringId, $survey, $due, $data) {
        $recurring = TableRegistry::get('Recurrings')->get($recurringId);
        $howLong = $survey->recurring_length;
        if (is_null($howLong)) $howLong = 26;
        $end = Time::now();

        switch($recurring->name) {
            case 'daily':
                if (!$due) $end = $end->addDays(1);
                $this->generateEvents($howLong * 7, 'day', 1, $end, $data);
                break;
            case 'weekly':
                if (!$due) $end = $end->addWeeks(1);
                $this->generateEvents($howLong, 'week', 1, $end, $data);
                break;
            case 'biweekly':
            case 'bimmonthly':
                if (!$due) $end = $end->addWeeks(2);
                $this->generateEvents($howLong / 2, 'week', 2, $end, $data);
                break;
            case 'monthly':
                if (!$due) $end = $end->addMonths(1);
                $this->generateEvents($howLong / 4, 'month', 1, $end, $data);
                break;
            case 'annually':
                if (!$due) $end = $end->addYears(1);
                $this->generateEvents($howLong / 52, 'year', 1, $end, $data);
                break;
        }
    }

    public function generateEvents($howLong, $interval, $add, $end, $data) {
        for ($i = 0; $i < $howLong; $i++) {
            $data->start = $end;
            $data->end = $end;
            switch ($interval) {
                case 'day':
                    $end->addDays($add);
                    break;
                case 'week':
                    $end->addWeeks($add);
                    break;
                case 'month':
                    $end->addMonths($add);
                    break;
                case 'year':
                    $end->addYears($add);
                    break;
            }
            $this->generateEvent($data);
        }
    }

    public function generateEvent($data) {
        $event = new Event('user.add.after', $this, $data);
        $this->eventManager()->dispatch($event);
    }
}
