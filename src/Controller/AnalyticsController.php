<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;


/**
 * Analytics Controller
 *
 * @property \App\Model\Table\AnalyticsTable $Analytics
 */
class AnalyticsController extends AppController
{
    public function retrieveAnalytics() {
        $analytics = $this->initializeAnalytics();
        $results = $this->getResults($analytics);
        $this->saveResults($results);
    }

    public function initializeAnalytics() {
        $KEY_FILE_LOCATION = __DIR__ . '/Analytics/service-account-credentials.json';
        $client = new \Google_Client();
        $client->setApplicationName("pcori");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new \Google_Service_Analytics($client);
        return $analytics;
    }

    function getResults($analytics) {
        $profile = $analytics->management_profiles->get('84595210', 'UA-84595210-3', '136970126');
        $results =  $analytics->data_ga->get(
            'ga:' . $profile->getId(),
            '7daysAgo',
            'today',
            'ga:users',
            [
               'dimensions' => 'ga:dimension1'
            ]);
        return $results;
    }

    function saveResults($results) {
        if (count($results->getRows()) > 0) {
            $rows = $results->getRows();
            foreach($rows as $row) {
                $result = $row[0];
                $resultArr = explode('//', $result);
                $user_id = $resultArr[0];
                $value = $resultArr[1];
                $date = $resultArr[2];
                $this->log($date);
                $analytics = TableRegistry::get('Analytics');
                $analytic = $analytics->find('all')->where([
                    'user_id' => $user_id,
                    'value' => $value,
                    'date' => $date
                ])->first();
                if (!$analytic) {
                    $analytic = $analytics->newEntity();
                    $analytic->date = $date;
                    $analytic->value = $value;
                    $analytic->user_id = $user_id;
                    $analytics->save($analytic);
                }
            }
        }
    }

}
