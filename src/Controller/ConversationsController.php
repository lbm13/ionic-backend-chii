<?php
namespace App\Controller;
use App\Service\MessageService;

/**
 * Conversations Controller
 *
 * @property \App\Model\Table\ConversationsTable $Conversations
 */
class ConversationsController extends AppController {
    public function get($id = null) {
        $id = $this->getData('id', $id);
        $page = $this->request->query['page'];

        $messageService = new MessageService();
        $conversations = $messageService->getConversation($id, $page);

        if (count($conversations) == 0) {
            $this->response->addMessage('End of pagination');
        } else {
            $this->response->addMessage($this->Conversations->table(), $conversations);
        }
    }
}
