<?php
namespace App\Controller;

use App\Model\Entity\User;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

/**
 * @property bool|object Settings
 */
class SettingsController extends AppController {
    const VALUE_TYPE_BOOLEAN = 'boolean';

    const SETTING_TYPES = [
        'group' => 'GroupsSettings',
        'project' => 'ProjectsSettings',
        'user' => 'SettingsUsers',
        'user_type' => 'SettingsUserTypes'
    ];

    /**
     * Get settings for the logged in user
     *
     * @param int|null $id
     *
     * @return void
     */
    public function get($id = null) {
        /** @var User $activeUser */
        $activeUser = $GLOBALS['user'];

        /** @var Query $query */
        $query = $this->Settings->find('all', []);
        $query = $query->contain([
            'Groups' => function(Query $q) use ($activeUser) {
                return $q->where(['Groups.id' => $activeUser->group_id]);
            },
            'Projects' => function(Query $q) use ($activeUser) {
                return $q->where(['Projects.id IN' => function() use ($activeUser) {
                    $projectIds = [];
                    foreach ($activeUser->projects as $project) {
                        $projectIds[] = $project->id;
                    }

                    return $projectIds;
                }]);
            },
            'Users' => function(Query $q) use ($activeUser) {
                return $q->where(['Users.id' => $activeUser->id]);
            },
            'UserTypes' => function(Query $q) use ($activeUser) {
                return $q->where(['UserTypes.id' => $activeUser->user_type_id]);
            }
        ]);

        $results = $query->toArray();
        $settings = [];
        foreach ($results as $setting) {
            if (!$this->isAtLeast('physician') && !in_array($setting->setting_type, ['user'])) {
                $setting = null;
            } else {
                $setting->setting_id = $setting->id;

                switch ($setting->setting_type) {
                    case 'group':
                        if (!is_null($activeUser->getGroup())) {
                            if (count($setting->groups) == 0) {
                                unset($setting->id);
                                $setting->group_id = $activeUser->getGroup()->id;
                                $setting->value = $setting->default_value;
                            } else {
                                $setting->id = $setting->groups[0]->_joinData->id;
                                $setting->group_id = $setting->groups[0]->_joinData->group_id;
                                $setting->value = $setting->groups[0]->_joinData->value;
                            }
                        } else {
                            $setting = null;
                        }
                        break;
                    case 'project':
                        if (count($setting->projects) == 0) {
                            $setting->value = $setting->default_value;
                        } else {
                            $setting->id = $setting->projects[0]->_joinData->id;
                            $setting->project_id = $setting->projects[0]->_joinData->project_id;
                            $setting->value = $setting->projects[0]->_joinData->value;
                        }
                        break;
                    case 'user':
                        if (count($setting->users) == 0) {
                            unset($setting->id);
                            $setting->value = $setting->default_value;
                        } else {
                            $setting->id = $setting->users[0]->_joinData->id;
                            $setting->value = $setting->users[0]->_joinData->value;
                        }
                        $setting->user_id = $activeUser->id;
                        break;
                    case 'user_type':
                        if (count($setting->user_types) == 0) {
                            $setting->value = $setting->default_value;
                        } else {
                            $setting->id = $setting->user_types[0]->_joinData->id;
                            $setting->user_type_id = $setting->user_types[0]->_joinData->user_type_id;
                            $setting->value = $setting->user_types[0]->_joinData->value;
                        }
                        break;
                    default:
                        continue;
                }
            }

            if (is_null($setting)) {
                unset($setting);
                continue;
            }

            if ($setting->value_type === self::VALUE_TYPE_BOOLEAN) {
                $setting->value = $setting->value == 'true' || $setting->value == '1';
            }

            unset($setting->groups);
            unset($setting->projects);
            unset($setting->users);
            unset($setting->user_types);

            unset($setting->created);
            unset($setting->modified);

            $settings[$setting->name] = $setting;
        }

        $this->response->addMessage('settings', $settings);
    }

    public function synchronize() {
        $this->update();
    }

    /**
     * Update the logged in user's settings
     */
    public function update() {
        $models = [];
        $settings = $this->getData('settings');
        if (!is_null($settings)) {
            if (count($settings) == 1) {
                $settings = $settings[0];
            }

            foreach ($settings as $setting) {
                $alias = (is_array($setting) && array_key_exists('setting_type', $setting)) ? self::SETTING_TYPES[$setting['setting_type']] : null;
                if (is_null($alias)) {
                    $this->response->addMessage('errors', 'Could not find setting type: '.$setting['setting_type']);
                } else {
                    $model = $this->loadModel($alias);
                    if (array_key_exists('id', $setting)) {
                        $object = $model->patchEntity($model->get($setting['id']), $setting);
                    } else {
                        $object = $model->newEntity($setting);
                    }

                    if (!array_key_exists($model->alias(), $models)) {
                        $models[$model->alias()] = [];
                    }
                    $models[$model->alias()][] = $object;
                }
            }

            foreach ($models as $alias => $objects) {
                $model = $this->loadModel($alias);
                if ($model->saveMany($objects)) {
                    $this->response->addMessage($model->table(), json_encode($objects));
                } else {
                    $this->response->addMessage('errors', 'Could not edit ' . $model->table() . '.');
                }
            }
        }

        $this->get();
    }
}
