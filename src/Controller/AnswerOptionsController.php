<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Network\HttpResponseCode;

/**
 * AnswerOptions Controller
 *
 * @property \App\Model\Table\AnswerOptionsTable $AnswerOptions
 */
class AnswerOptionsController extends AppController {
    public function add() {
        $optionModel = $this->loadModel('AnswerOptions');
        $questionId = $this->getData('question_id');
        $position = 1;
        $optionWithHighestPosition = $optionModel->find('all', [
            'order' => ['position' => 'desc'],
            'conditions' => ['question_id' => $questionId]
        ])->first();
        if ($optionWithHighestPosition != null) $position = $optionWithHighestPosition->position + 1;
        $_POST['position'] = $position;
        if (array_key_exists('image', $_POST)) {
            unset($_POST['image']);
        }
        parent::add();
    }
}
