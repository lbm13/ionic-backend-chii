<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CommentsVideos Controller
 *
 * @property \App\Model\Table\CommentsVideosTable $CommentsVideos
 */
class CommentsVideosController extends AppController {
    public $paginate = [
        'limit' => 25,
        'order' => [
            'CommentsVideos.created' => 'desc'
        ]
    ];

    public function initialize() {
        parent::initialize();

        $this->loadComponent('Paginator');
    }

    public function page() {
        $this->set('comments_videos', $this->paginate());
        $this->set('_serialize', ['messages']);
    }

    public function add() {
        if (parent::usingToken()) {
            parent::add();
        }
    }
}
