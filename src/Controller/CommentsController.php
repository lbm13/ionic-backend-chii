<?php
namespace App\Controller;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 */
class CommentsController extends AppController {
    public function add() {
        if (parent::usingToken()) {
            parent::add();
        }
    }
}
