<?php
namespace App\Controller;

use App\Service\PostService;
use Cake\Event\Event;

class PostsController extends AppController {
    public $paginate = [
        'limit' => 10,
        'order' => [
            'Post.created' => 'DESC'
        ]
    ];

    public function initialize() {
        parent::initialize();

        $this->loadComponent('Paginator');
    }

    public function add() {
        if (parent::usingToken('from_user_id')) {
            $post = parent::add();

            $event = new Event('post.add.after', $this, $post);
            $this->eventManager()->dispatch($event);
        }
    }

    public function delete($id = null) {
        $id = $this->getData('id', $id);
        $delete = $this->isAtLeast('projectCoordinator');
        if (!$delete) {
            $postService = new PostService();
            $post = $postService->getById($id);
            if ($post) {
                if ($post->from_user_id !== $GLOBALS['user']->id && $post->to_user_ud !== $GLOBALS['user']->id) {
                    $this->response->addError('User does not have permission to delete post.');
                    $this->response->kill();
                }
            } else {
                $this->response->addError('Could not find post with id '.$id);
                $this->response->kill();
            }
        }

        parent::delete($id);
    }
}
