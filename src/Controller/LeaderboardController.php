<?php
namespace App\Controller;

use App\Model\Entity\FitbitUser;
use Cake\ORM\TableRegistry;

/**
 * LeaderboardController Controller
 */
class LeaderboardController extends AppController {
    private function buildSorter($key) {
        return function($a, $b) use ($key) {
            return strnatcmp($a[$key], $b[$key]);
        };
    }

    public function get($type = null, $projectId = null, $startDate = null, $endDate = null) {
        if (is_null($type)) {
            $type = 'steps';
        }

        if (is_null($projectId)) {
            $this->response->addError('ProjectId is required.');
        } else {
            $query = TableRegistry::get('FitbitUsers')->find('all')->distinct();
            $query->contain(['FitbitActivities']);

            $dateParams = [];
            if (!is_null($startDate)) {
                $dateParams['start_time >'] = $startDate;
            }
            if (!is_null($endDate)) {
                $dateParams['start_time <'] = $endDate;
            }

            if (count($dateParams) > 0) {
                $query->innerJoinWith(
                    'FitbitActivities',
                    function($q) use ($dateParams) {
                        return $q->where($dateParams);
                    }
                );
            }

            $query->contain(['Users.Images']);
            $query->innerJoinWith(
                'Users.Projects',
                function($q) use ($projectId) {
                    return $q->where(['Projects.id' => $projectId]);
                }
            );

            /** @var FitbitUser[] $fitbitUsers */
            $fitbitUsers = $query->toArray();
            foreach ($fitbitUsers as $fitbitUser) {
                foreach ($fitbitUser->fitbit_activities as $fitbitActivity) {
                    $fitbitUser->total += $fitbitActivity->$type;
                }
            }

            usort($fitbitUsers, $this->buildSorter('total'));

            $positions = [];
            $positionNumber = count($fitbitUsers);
            foreach ($fitbitUsers as $fitbitUser) {
                $fitbitUser->position = $positionNumber;

                array_unshift($positions, $fitbitUser);

                $positionNumber--;
            }

            $this->response->addMessage('leaderboard', ['positions' => $positions]);
        }
    }
}