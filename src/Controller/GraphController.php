<?php
namespace App\Controller;

use App\Model\Entity\Response;
use App\Service\GraphService;

class GraphController extends AppController {
    /**
     * Generates data for a discrete bar chart
     */
    public function discreteBarChart() {
        $graphService = new GraphService();
        $this->response->addMessage('data', $graphService->getLoginData());
    }
}
