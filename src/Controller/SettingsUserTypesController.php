<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Network\HttpResponseCode;
use Cake\Event\Event;

/**
 * SettingsUserTypes Controller
 *
 * @property \App\Model\Table\SettingsUserTypesTable $SettingsUserTypes
 */
class SettingsUserTypesController extends AppController {
    public function beforeFilter(Event $event) {
        $this->response->addError('You must use the settings controller.', HttpResponseCode::RESPONSE_NOT_ALLOWED);
        $this->response->kill();
    }
}
