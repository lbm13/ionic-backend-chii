<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Response;
use App\Network\HttpResponseCode;
use App\Service\FollowerService;

/**
 * Followers Controller
 *
 * @property \App\Model\Table\FollowersTable $Followers
 */
class FollowersController extends AppController {
    public function add() {
        if (parent::usingToken()) {
            parent::add();
        }
    }

    public function follow($followingId = null) {
        $followerService = new FollowerService();
        $follower = $followerService->follow($GLOBALS['user']->id, $this->getData('following_id', $followingId));
        $this->response->addMessage($this->Followers->table(), $follower);
    }

    public function unfollow($followingId = null) {
        $followerService = new FollowerService();
        if ($followerService->unfollow($GLOBALS['user']->id, $this->getData('following_id', $followingId))) {
            $this->response->addMessage('messages', 'Successfully deleted.', HttpResponseCode::RESPONSE_DELETED);
        } else {
            $this->response->addError('Could not delete the following.', HttpResponseCode::RESPONSE_INTERNAL_SERVER_ERROR);
        }
    }
}
