<?php
namespace App\Controller;

use App\Network\HttpResponseCode;
use App\Service\SurveyService;

/**
 * Answers Controller
 *
 * @property \App\Model\Table\AnswersTable $Answers
 */
class AnswersController extends AppController {
    /**
     * Add method
     */
    public function add() {
        if (parent::usingToken()) {
            parent::add();
        }
    }
}
