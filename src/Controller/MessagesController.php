<?php
namespace App\Controller;

use App\Model\Entity\Response;
use App\Network\HttpResponseCode;
use App\Service\MessageService;
use Cake\ORM\TableRegistry;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 */
class MessagesController extends AppController {
    public function get($id = null) {
        $messageService = new MessageService();

        $messages = $messageService->getMessages($this->getData('conversation_id', $id));

        $messageService->updateReadStatus($messages);

        $this->response->addMessage($this->loadModel()->table(), $messages);
    }

    public function send() {
        $conversation_id = $this->getData('conversation_id');
        if (is_null($conversation_id)) {
            $conversationModel = $this->loadModel('Conversations');
            $conversation = $conversationModel->save($conversationModel->newEntity([]));
            if ($conversation) {
                $conversation_id = $conversation->id;
            } else {
                $this->response->addError('Could not create nor find conversation.');
                $this->response->kill();
            }
        }

        $message = $this->getData('message');
        if (is_null($message)) {
            $this->response->addError('Message required.', HttpResponseCode::RESPONSE_INVALID_FIELD);
        }

        $recipients = $this->getData('recipients');
        if (is_null($recipients)) {
            $messageService = new MessageService();
            $recipients = $messageService->getResponseRecipients($conversation_id);
        }

        $messageModel = $this->loadModel();
        $message = $messageModel->save(
            $messageModel->newEntity(
                [
                    'conversation_id' => $conversation_id,
                    'message' => $message,
                    'recipients' => $recipients,
                    'sender_id' => $GLOBALS['user']->id
                ],
                [
                    'associated' => true
                ]
            )
        );

        if ($message) {
            $messageService = new MessageService();
            $this->response->addMessage('conversation', $messageService->getConversation($conversation_id));
        } else {
            $this->response->addError('Could not create the message.');
        }
    }
}
