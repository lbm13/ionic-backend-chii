<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PostLikes Controller
 *
 * @property \App\Model\Table\PostLikesTable $PostLikes
 */
class PostLikesController extends AppController {
    public function add($id = null) {
        if (parent::usingToken()) {
            parent::add();
        }
    }

    public function delete($id = null) {
        if (parent::usingToken()) {
            parent::delete();
        }
    }
}
