<?php
namespace App\Model\Table;

use App\Model\Entity\Tag;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tags Model
 *
 * @property \Cake\ORM\Association\HasMany $Flags
 * @property \Cake\ORM\Association\BelongsToMany $AnswerOptions
 * @property \Cake\ORM\Association\BelongsToMany $Tips
 * @property \Cake\ORM\Association\BelongsToMany $Videos
 * @property \Cake\ORM\Association\BelongsToMany $TagGroups
 */
class TagsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tags');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Flags', [
            'foreignKey' => 'tag_id'
        ]);
        $this->belongsToMany('AnswerOptions', [
            'foreignKey' => 'tag_id',
            'targetForeignKey' => 'answer_option_id',
            'joinTable' => 'answer_options_tags'
        ]);
        $this->belongsToMany('Tips', [
            'foreignKey' => 'tag_id',
            'targetForeignKey' => 'tip_id',
            'joinTable' => 'tags_tips'
        ]);
        $this->belongsToMany('Videos', [
            'foreignKey' => 'tag_id',
            'targetForeignKey' => 'video_id',
            'joinTable' => 'tags_videos'
        ]);
        $this->belongsToMany('TagGroups', [
            'foreignKey' => 'tag_id',
            'targetForeignKey' => 'tag_group_id',
            'joinTable' => 'tag_groups_tags'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));
        return $rules;
    }
}
