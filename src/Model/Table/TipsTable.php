<?php
namespace App\Model\Table;

use App\Model\Entity\Tip;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tips Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $AnswerOptions
 * @property \Cake\ORM\Association\BelongsToMany $Resources
 * @property \Cake\ORM\Association\BelongsToMany $Tags
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class TipsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tips');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('AnswerOptions', [
            'foreignKey' => 'tip_id',
            'targetForeignKey' => 'answer_option_id',
            'joinTable' => 'answer_options_tips'
        ]);
        $this->belongsToMany('Resources', [
            'foreignKey' => 'tip_id',
            'targetForeignKey' => 'resource_id',
            'joinTable' => 'resources_tips'
        ]);
        $this->belongsToMany('Tags', [
            'foreignKey' => 'tip_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'tags_tips'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'tip_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'tips_users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('tip', 'create')
            ->notEmpty('tip');

        $validator
            ->allowEmpty('description');

        return $validator;
    }
}
