<?php
namespace App\Model\Table;

use App\Model\Entity\FitbitHeartRateZone;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FitbitHeartRateZones Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FitbitActivities
 */
class FitbitHeartRateZonesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fitbit_heart_rate_zones');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('FitbitActivities', [
            'foreignKey' => 'fitbit_activity_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('max')
            ->allowEmpty('max');

        $validator
            ->integer('min')
            ->allowEmpty('min');

        $validator
            ->integer('minutes')
            ->allowEmpty('minutes');

        $validator
            ->allowEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fitbit_activity_id'], 'FitbitActivities'));
        return $rules;
    }
}
