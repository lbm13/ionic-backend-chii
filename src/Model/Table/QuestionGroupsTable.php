<?php
namespace App\Model\Table;

use App\Model\Entity\QuestionGroup;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * QuestionGroups Model
 *
 * @property \Cake\ORM\Association\HasMany $Questions
 */
class QuestionGroupsTable extends Table {

}
