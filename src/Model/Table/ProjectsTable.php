<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Projects Model
 *
 * @property \Cake\ORM\Association\HasMany $FitbitAuth
 * @property \Cake\ORM\Association\HasMany $Groups
 * @property \Cake\ORM\Association\BelongsToMany $Badges
 * @property \Cake\ORM\Association\BelongsToMany $Operations
 * @property \Cake\ORM\Association\BelongsToMany $Settings
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class ProjectsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('projects');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('FitbitAuth', [
            'foreignKey' => 'project_id'
        ]);
        $this->hasMany('Groups', [
            'foreignKey' => 'project_id'
        ]);
        $this->belongsToMany('Badges', [
            'foreignKey' => 'project_id',
            'targetForeignKey' => 'badge_id',
            'joinTable' => 'badges_projects'
        ]);
        $this->belongsToMany('Operations', [
            'foreignKey' => 'project_id',
            'targetForeignKey' => 'operation_id',
            'joinTable' => 'operations_projects'
        ]);
        $this->belongsToMany('Settings', [
            'foreignKey' => 'project_id',
            'targetForeignKey' => 'setting_id',
            'joinTable' => 'projects_settings'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'project_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'projects_users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));
        return $rules;
    }
}
