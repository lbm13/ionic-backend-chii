<?php
namespace App\Model\Table;

use App\Model\Entity\FitbitActivity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FitbitActivities Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FitbitUsers
 * @property \Cake\ORM\Association\HasMany $FitbitHeartRateZones
 */
class FitbitActivitiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fitbit_activities');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('FitbitUsers', [
            'foreignKey' => 'fitbit_user_id'
        ]);
        $this->hasMany('FitbitHeartRateZones', [
            'foreignKey' => 'fitbit_activity_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('activity_name');

        $validator
            ->integer('average_heart_rate')
            ->allowEmpty('average_heart_rate');

        $validator
            ->integer('calories')
            ->allowEmpty('calories');

        $validator
            ->numeric('distance')
            ->allowEmpty('distance');

        $validator
            ->integer('duration')
            ->allowEmpty('duration');

        $validator
            ->dateTime('last_modified')
            ->allowEmpty('last_modified');

        $validator
            ->numeric('speed')
            ->allowEmpty('speed');

        $validator
            ->dateTime('start_time')
            ->allowEmpty('start_time');

        $validator
            ->integer('steps')
            ->allowEmpty('steps');

        $validator
            ->integer('active_duration')
            ->allowEmpty('active_duration');

        $validator
            ->allowEmpty('heart_rate_link');

        $validator
            ->allowEmpty('log_type');

        $validator
            ->allowEmpty('tcx_link');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fitbit_user_id'], 'FitbitUsers'));
        return $rules;
    }
}
