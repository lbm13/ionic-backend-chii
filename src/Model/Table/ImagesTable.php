<?php
namespace App\Model\Table;

use App\Model\Entity\Image;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Images Model
 *
 * @property \Cake\ORM\Association\HasMany $AnswerOptions
 * @property \Cake\ORM\Association\HasMany $Badges
 * @property \Cake\ORM\Association\HasMany $Notifications
 * @property \Cake\ORM\Association\HasMany $Playlists
 * @property \Cake\ORM\Association\HasMany $Posts
 * @property \Cake\ORM\Association\HasMany $Questions
 * @property \Cake\ORM\Association\HasMany $Users
 * @property \Cake\ORM\Association\HasMany $Thumbnails
 * @property \Cake\ORM\Association\BelongsToMany $Videos
 */
class ImagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('images');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('AnswerOptions', [
            'foreignKey' => 'image_id'
        ]);
        $this->hasMany('Badges', [
            'foreignKey' => 'image_id'
        ]);
        $this->hasMany('Notifications', [
            'foreignKey' => 'image_id'
        ]);
        $this->hasMany('Playlists', [
            'foreignKey' => 'image_id'
        ]);
        $this->hasMany('Posts', [
            'foreignKey' => 'image_id'
        ]);
        $this->hasMany('Questions', [
            'foreignKey' => 'image_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'image_id'
        ]);
        $this->hasMany('Thumbanils', [
            'className' => 'Videos',
            'foreignKey' => 'image_id'
        ]);
        $this->belongsToMany('Videos', [
            'foreignKey' => 'image_id',
            'targetForeignKey' => 'video_id',
            'joinTable' => 'images_videos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('file_path', 'create')
            ->notEmpty('file_path');

        $validator
            ->allowEmpty('title');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }
}
