<?php
namespace App\Model\Table;

use App\Model\Entity\GraidConstruct;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GraidConstructs Model
 *
 * @property \Cake\ORM\Association\HasMany $ChiiQuestions
 * @property \Cake\ORM\Association\HasMany $Graids
 */
class GraidConstructsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('graid_constructs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('ChiiQuestions', [
            'foreignKey' => 'graid_construct_id'
        ]);
        $this->hasMany('Graids', [
            'foreignKey' => 'graid_construct_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('construct', 'create')
            ->notEmpty('construct');

        return $validator;
    }
}
