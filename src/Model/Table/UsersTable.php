<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $UserTypes
 * @property \Cake\ORM\Association\BelongsTo $Groups
 * @property \Cake\ORM\Association\BelongsTo $Images
 * @property \Cake\ORM\Association\HasMany $Comments
 * @property \Cake\ORM\Association\HasMany $Events
 * @property \Cake\ORM\Association\HasMany $FitbitUsers
 * @property \Cake\ORM\Association\HasMany $Followers
 * @property \Cake\ORM\Association\HasMany $Notifications
 * @property \Cake\ORM\Association\HasMany $PostLikes
 * @property \Cake\ORM\Association\HasMany $ReportedComments
 * @property \Cake\ORM\Association\HasMany $ReportedPosts
 * @property \Cake\ORM\Association\HasMany $VideoLikes
 * @property \Cake\ORM\Association\BelongsToMany $Badges
 * @property \Cake\ORM\Association\BelongsToMany $eusers
 * @property \Cake\ORM\Association\BelongsToMany $Projects
 * @property \Cake\ORM\Association\BelongsToMany $Settings
 * @property \Cake\ORM\Association\BelongsToMany $Surveys
 * @property \Cake\ORM\Association\BelongsToMany $Tips
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('UserTypes', [
            'foreignKey' => 'user_type_id'
        ]);
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id'
        ]);
        $this->belongsTo('Images', [
            'foreignKey' => 'image_id'
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('FitbitUsers', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Followers', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Notifications', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PostLikes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ReportedComments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ReportedPosts', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('VideoLikes', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsToMany('Badges', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'badge_id',
            'joinTable' => 'badges_users'
        ]);
        $this->belongsToMany('Projects', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'project_id',
            'joinTable' => 'projects_users'
        ]);
        $this->belongsToMany('Settings', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'setting_id',
            'joinTable' => 'settings_users'
        ]);
        $this->belongsToMany('Surveys', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'survey_id',
            'joinTable' => 'surveys_users'
        ]);
        $this->belongsToMany('Events', [
            'className' => 'Events',
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'event_id',
            'joinTable' => 'events_users'
        ]);
        $this->belongsToMany('Tips', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'tip_id',
            'joinTable' => 'tips_users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->allowEmpty('first_name');

        $validator
            ->allowEmpty('last_name');

        $validator
            ->allowEmpty('email');

        $validator
            ->integer('logout_count')
            ->allowEmpty('logout_count');

        $validator
            ->integer('login_count')
            ->allowEmpty('login_count');

        $validator
            ->allowEmpty('security_question');

        $validator
            ->allowEmpty('security_answer');

        $validator
            ->boolean('gender')
            ->allowEmpty('gender');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['user_type_id'], 'UserTypes'));
        $rules->add($rules->existsIn(['group_id'], 'Groups'));
        $rules->add($rules->existsIn(['image_id'], 'Images'));
        return $rules;
    }
}
