<?php
namespace App\Model\Table;

use App\Model\Entity\Setting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Settings Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Groups
 * @property \Cake\ORM\Association\BelongsToMany $Projects
 * @property \Cake\ORM\Association\BelongsToMany $UserTypes
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class SettingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('settings');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Groups', [
            'foreignKey' => 'setting_id',
            'targetForeignKey' => 'group_id',
            'joinTable' => 'groups_settings'
        ]);
        $this->belongsToMany('Projects', [
            'foreignKey' => 'setting_id',
            'targetForeignKey' => 'project_id',
            'joinTable' => 'projects_settings'
        ]);
        $this->belongsToMany('UserTypes', [
            'foreignKey' => 'setting_id',
            'targetForeignKey' => 'user_type_id',
            'joinTable' => 'settings_user_types'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'setting_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'settings_users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('display_name', 'create')
            ->notEmpty('display_name');

        $validator
            ->allowEmpty('description');

        $validator
            ->requirePresence('default_value', 'create')
            ->notEmpty('default_value');

        $validator
            ->requirePresence('value_type', 'create')
            ->notEmpty('value_type');

        $validator
            ->requirePresence('setting_type', 'create')
            ->notEmpty('setting_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        return $rules;
    }
}
