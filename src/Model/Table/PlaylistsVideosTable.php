<?php
namespace App\Model\Table;

use App\Model\Entity\PlaylistsVideo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * PlaylistsVideos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Playlists
 * @property \Cake\ORM\Association\BelongsTo $Videos
 */
class PlaylistsVideosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('playlists_videos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Playlists', [
            'foreignKey' => 'playlist_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Videos', [
            'foreignKey' => 'video_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['playlist_id'], 'Playlists'));
        $rules->add($rules->existsIn(['video_id'], 'Videos'));
        return $rules;
    }

    public function beforeSave($event, $entity, $options) {
        foreach($_POST['videos'] as $video) {
            if ($video['id'] == $entity->video_id) {
                $entity->position = $video['_joinData']['position'];
                return;
            }
        }
    }
}
