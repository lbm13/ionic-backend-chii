<?php
namespace App\Model\Table;

use App\Model\Entity\Resource;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Resources Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Graids
 * @property \Cake\ORM\Association\BelongsToMany $Tips
 */
class ResourcesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('resources');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Graids', [
            'foreignKey' => 'resource_id',
            'targetForeignKey' => 'graid_id',
            'joinTable' => 'graids_resources'
        ]);
        $this->belongsToMany('Tips', [
            'foreignKey' => 'resource_id',
            'targetForeignKey' => 'tip_id',
            'joinTable' => 'resources_tips'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('url');

        return $validator;
    }
}
