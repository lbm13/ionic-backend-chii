<?php
namespace App\Model\Table;

use App\Model\Entity\Post;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Posts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ToUsers
 * @property \Cake\ORM\Association\BelongsTo $FromUsers
 * @property \Cake\ORM\Association\HasMany $PostLikes
 * @property \Cake\ORM\Association\HasMany $ReportedPosts
 * @property \Cake\ORM\Association\BelongsToMany $Comments
 */
class PostsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('posts');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('FromUser', [
            'className'  => 'Users',
            'foreignKey' => 'from_user_id',
            'propertyName' => 'from_user'
        ]);
        $this->belongsTo('ToUser', [
            'className'  => 'Users',
            'foreignKey' => 'to_user_id',
            'propertyName' => 'to_user'
        ]);
        $this->hasMany('PostLikes', [
            'foreignKey' => 'post_id'
        ]);
        $this->belongsTo('Images', [
            'foreignKey' => 'image_id'
        ]);
        $this->hasMany('ReportedPosts', [
            'foreignKey' => 'post_id'
        ]);
        $this->belongsToMany('Comments', [
            'foreignKey' => 'post_id',
            'targetForeignKey' => 'comment_id',
            'joinTable' => 'comments_posts'
        ]);
        $this->belongsToMany('Images', [
            'foreignKey' => 'post_id',
            'targetForeignKey' => 'image_id',
            'joinTable' => 'posts_images'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->dateTime('stickied')
            ->allowEmpty('stickied');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['from_user_id'], 'FromUser'));
        $rules->add($rules->existsIn(['to_user_id'], 'ToUser'));
        $rules->add($rules->existsIn(['image_id'], 'Images'));
        return $rules;
    }
}
