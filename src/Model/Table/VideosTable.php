<?php
namespace App\Model\Table;

use App\Model\Entity\Video;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Videos Model
 *
 * @property \Cake\ORM\Association\HasMany $VideoLikes
 * @property \Cake\ORM\Association\BelongsToMany $Comments
 * @property \Cake\ORM\Association\BelongsToMany $Images
 * @property \Cake\ORM\Association\BelongsTo $Thumbnails
 * @property \Cake\ORM\Association\BelongsToMany $Playlists
 * @property \Cake\ORM\Association\BelongsToMany $Tags
 */
class VideosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('videos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('VideoLikes', [
            'foreignKey' => 'video_id'
        ]);
        $this->belongsToMany('Comments', [
            'foreignKey' => 'video_id',
            'targetForeignKey' => 'comment_id',
            'joinTable' => 'comments_videos'
        ]);
        $this->belongsToMany('Images', [
            'foreignKey' => 'video_id',
            'targetForeignKey' => 'image_id',
            'joinTable' => 'images_videos'
        ]);

        $this->belongsTo('Thumbnails', [
            'foreignKey' => 'image_id',
            'className' => 'Images',
            'foreignKey' => 'image_id'
        ]);

        $this->belongsToMany('Playlists', [
            'foreignKey' => 'video_id',
            'targetForeignKey' => 'playlist_id',
            'joinTable' => 'playlists_videos'
        ]);

        $this->belongsToMany('Tags', [
            'foreignKey' => 'video_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'tags_videos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('file_path', 'create')
            ->notEmpty('file_path');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('title');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        return $rules;
    }
}
