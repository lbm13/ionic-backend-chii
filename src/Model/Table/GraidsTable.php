<?php
namespace App\Model\Table;

use App\Model\Entity\Graid;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Graids Model
 *
 * @property \Cake\ORM\Association\BelongsTo $GraidTypes
 * @property \Cake\ORM\Association\BelongsTo $GraidNumbers
 * @property \Cake\ORM\Association\BelongsTo $GraidConstructs
 * @property \Cake\ORM\Association\BelongsToMany $Resources
 */
class GraidsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('graids');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('GraidTypes', [
            'foreignKey' => 'graid_type_id'
        ]);
        $this->belongsTo('GraidNumbers', [
            'foreignKey' => 'graid_number_id'
        ]);
        $this->belongsTo('GraidConstructs', [
            'foreignKey' => 'graid_construct_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Resources', [
            'foreignKey' => 'graid_id',
            'targetForeignKey' => 'resource_id',
            'joinTable' => 'graids_resources'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('text');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['graid_type_id'], 'GraidTypes'));
        $rules->add($rules->existsIn(['graid_number_id'], 'GraidNumbers'));
        $rules->add($rules->existsIn(['graid_construct_id'], 'GraidConstructs'));
        return $rules;
    }
}
