<?php
namespace App\Model\Entity;

use App\Service\QuestionService;
use Cake\ORM\Entity;

/**
 * Question Entity.
 *
 * @property int $id
 * @property int $position
 * @property string $phrasing
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $survey_id
 * @property \App\Model\Entity\Survey $survey
 * @property int $answer_input_type_id
 * @property \App\Model\Entity\AnswerInputType $answer_input_type
 * @property bool $required
 * @property int $image_id
 * @property \App\Model\Entity\Image $image
 * @property \App\Model\Entity\AnswerOption[] $answer_options
 * @property \App\Model\Entity\Answer[] $answers
 */
class Question extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Get the related AnswerInputType
     *
     * @return AnswerInputType
     */
    public function getAnswerInputType() {
        $questionService = new QuestionService();
        return $questionService->getAnswerInputType($this);
    }
}
