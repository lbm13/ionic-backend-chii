<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Event Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $start
 * @property \Cake\I18n\Time $end
 * @property string $title
 * @property string $description
 * @property int $creator_id
 * @property \App\Model\Entity\Creator $creator
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property bool $allDay
 * @property \App\Model\Entity\User[] $users
 */
class Event extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected $_virtual = [
        'is_creator'
    ];

    public function _getIsCreator() {
        return $this->creator_id === $GLOBALS['user']->id;
    }
}
