<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tag Entity.
 *
 * @property int $id
 * @property string $name
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\VideoTag[] $video_tags
 */
class Upload extends Entity
{
    const UPLOAD_DIRECTORY = '/opt/lampp/htdocs/ImageUpload/images/';

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
    
    public function move($file) {
        $fileName = $file['name'];
        $temp = explode(".", $fileName);
        $newfilename = substr(md5($file['tmp_name']), 0, 10) . '.' . end($temp);
        $filePath = self::UPLOAD_DIRECTORY . $newfilename;
        move_uploaded_file($file['tmp_name'], $filePath);
        return $filePath;
    }
}

