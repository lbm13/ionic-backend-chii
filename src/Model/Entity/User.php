<?php
namespace App\Model\Entity;

use App\Service\UserService;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity.
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property int $user_type_id
 * @property \App\Model\Entity\UserType $user_type
 * @property string $email
 * @property int $logout_count
 * @property int $group_id
 * @property \App\Model\Entity\Group $group
 * @property int $login_count
 * @property string $security_question
 * @property string $security_answer
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $image_id
 * @property \App\Model\Entity\Image $image
 * @property bool $gender
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Event[] $events
 * @property \App\Model\Entity\FitbitUser[] $fitbit_users
 * @property \App\Model\Entity\Follower[] $followers
 * @property \App\Model\Entity\Notification[] $notifications
 * @property \App\Model\Entity\PostLike[] $post_likes
 * @property \App\Model\Entity\ReportedComment[] $reported_comments
 * @property \App\Model\Entity\ReportedPost[] $reported_posts
 * @property \App\Model\Entity\VideoLike[] $video_likes
 * @property \App\Model\Entity\Badge[] $badges
 * @property \App\Model\Entity\Project[] $projects
 * @property \App\Model\Entity\Setting[] $settings
 * @property \App\Model\Entity\Survey[] $surveys
 */
class User extends Entity {
    const MINIMUM_PASSWORD_LENGTH = 3;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Fields that are excluded from JSON an array versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'security_answer'
    ];

    protected $_virtual = [
        'is_friend',
        'is_follower',
        'is_following',
        'is_online',
        'full_name'
    ];

    public function _getIsFollower() {
        $userService = new UserService();
        return $userService->isFollower($this);
    }

    public function _getIsFollowing() {
        $userService = new UserService();
        return $userService->isFollowing($this);
    }

    public function _getIsFriend() {
        $isFollower = $this->_getIsFollower();
        return (is_null($isFollower)) ? null : $isFollower && $this->_getIsFollowing();
    }

    public function _getIsOnline() {
        return $this->login_count > $this->logout_count;
    }

    public function _getFullName() {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * Hash and set user's password
     *
     * @param $password
     *
     * @return bool|string
     */
    protected function _setPassword($password) {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($password);
    }

    /**
     * Return the association Group
     *
     * @return Group
     */
    public function getGroup() {
        $userService = new UserService();
        return $userService->getGroup($this);
    }

    /**
     * Return the association Project
     *
     * @return Project[]
     */
    public function getProjects() {
        $userService = new UserService();
        return $userService->getProjects($this);
    }

    /**
     * Return the association User Type
     *
     * @return UserType
     */
    public function getUserType() {
        $userService = new UserService();
        return $userService->getUserType($this);
    }
}
