<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Graid Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $graid_type_id
 * @property \App\Model\Entity\GraidType $graid_type
 * @property int $graid_number_id
 * @property \App\Model\Entity\GraidNumber $graid_number
 * @property string $text
 * @property int $graid_construct_id
 * @property \App\Model\Entity\GraidConstruct $graid_construct
 * @property \App\Model\Entity\Resource[] $resources
 */
class Graid extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
