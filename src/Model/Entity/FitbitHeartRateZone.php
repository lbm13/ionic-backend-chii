<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FitbitHeartRateZone Entity.
 *
 * @property int $id
 * @property int $fitbit_activity_id
 * @property \App\Model\Entity\FitbitActivity $fitbit_activity
 * @property int $max
 * @property int $min
 * @property int $minutes
 * @property string $name
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class FitbitHeartRateZone extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
