<?php
namespace App\Model\Entity;

use App\Service\SurveyService;
use Cake\ORM\Entity;

/**
 * Survey Entity.
 *
 * @property int $id
 * @property string $name
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 * @property int $recurring_id
 * @property \App\Model\Entity\Recurring $recurring
 * @property int $project_id
 * @property \Cake\I18n\Time $due
 * @property \App\Model\Entity\Project $project
 * @property bool $requiredOnStart
 * @property \App\Model\Entity\Question[] $questions
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\SurveysUser[] $surveys_users
 */
class Survey extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected $_virtual = [
        'is_complete',
        'response_count'
    ];

    protected function _getIsComplete() {
        $surveyService = new SurveyService();
        return $surveyService->hasCompleted($this, $GLOBALS['user']);
    }

    protected function _getResponseCount() {
        $surveyService = new SurveyService();
        return $surveyService->getResponseCount($this->id, $GLOBALS['user']->id);
    }

    public function getAnswers($userId, $responseOrder) {
        $surveyService = new SurveyService();
        return $surveyService->getAnswers($this, $userId, $responseOrder);
    }
}
