<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FitbitDevice Entity.
 *
 * @property int $id
 * @property int $battery
 * @property string $device_version
 * @property int $device_id
 * @property \Cake\I18n\Time $last_sync_time
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\FitbitUser[] $fitbit_users
 */
class FitbitDevice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
