<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FitbitUser Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property string $refresh_token
 * @property string $fitbit_id
 * @property int $fitbit_device_id
 * @property \App\Model\Entity\FitbitDevice $fitbit_device
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\FitbitActivity[] $fitbit_activities
 */
class FitbitUser extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
