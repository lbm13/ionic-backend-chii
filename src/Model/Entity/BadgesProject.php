<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BadgesProject Entity.
 *
 * @property int $id
 * @property int $badge_id
 * @property \App\Model\Entity\Badge $badge
 * @property int $project_id
 * @property string $reward_rule_key
 * @property string $reward_rule_value
 * @property \App\Model\Entity\Project $project
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class BadgesProject extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
