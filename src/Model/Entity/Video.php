<?php
namespace App\Model\Entity;

use App\Service\VideoService;
use Cake\ORM\Entity;

/**
 * Video Entity.
 *
 * @property int $id
 * @property string $file_path
 * @property int $image_id
 * @property string $description
 * @property string $title
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\VideoLike[] $video_likes
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Image[] $images
 * @property \App\Model\Entity\Playlist[] $playlists
 * @property \App\Model\Entity\Reaction[] $reactions
 * @property \App\Model\Entity\Tag[] $tags
 */
class Video extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected $_virtual = [
        'like_count',
        'has_liked'
    ];

    /**
     * Get the virtual field likeCount
     *
     * @return int
     */
    protected function _getLikeCount() {
        $videoService = new VideoService();
        return $videoService->getLikeCount($this);
    }

    /**
     * Get the virtual field hasLiked
     *
     * @return bool
     */
    protected function _getHasLiked() {
        $videoService = new VideoService();
        return $videoService->hasLiked($this);
    }
}
