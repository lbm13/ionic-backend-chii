<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Answer Entity.
 *
 * @property int $id
 * @property string $response
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $answer_option_id
 * @property \App\Model\Entity\AnswerOption $answer_option
 * @property int $surveys_users_id
 * @property \App\Model\Entity\SurveysUser $surveys_user
 * @property int $question_id
 * @property \App\Model\Entity\Question $question
 */
class Answer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
