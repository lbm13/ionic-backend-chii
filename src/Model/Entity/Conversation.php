<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Conversation Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Message[] $messages
 */
class Conversation extends Entity {
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected $_virtual = [
        'last_message',
        'unread_messages'
    ];

    public function _getLastMessage() {
        $messagesTable = TableRegistry::get('Messages');
        $query = $messagesTable->find('all');
        $query = $query->contain(['Recipients', 'Senders']);
        $query = $query->where(['Messages.conversation_id' => $this->id]);
        $query = $query->orderDesc('Messages.created');

        return $query->first();
    }

    public function _getUnreadMessages() {
        $messagesTable = TableRegistry::get('Messages');
        $query = $messagesTable->find('all');
        $query = $query->innerJoinWith(
            'MessagesRecipients',
            function ($q) {
                return $q->where([
                    'has_read' => false,
                    'recipient_id' => $GLOBALS['user']->id
                ]);
            }
        );
        $query = $query->where([
            'Messages.conversation_id' => $this->id,
        ]);

        return $query->count();
    }
}
