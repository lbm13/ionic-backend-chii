<?php
namespace App\Model\Entity;

use App\Model\Table\PostsTable;
use App\Service\PostService;
use Cake\ORM\Entity;

/**
 * Post Entity.
 *
 * @property int $id
 * @property int $from_user_id
 * @property \App\Model\Entity\User $fromUser
 * @property int $to_user_id
 * @property \App\Model\Entity\User $toUser
 * @property string $message
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $stickied
 * @property int $image_id
 * @property \App\Model\Entity\PostLike[] $post_likes
 * @property \App\Model\Entity\Comment[] $comments
 */
class Post extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected $_virtual = [
        'like_count',
        'has_liked',
        'has_flagged',
        'is_stickied'
    ];

    /**
     * Get the virtual field likeCount
     *
     * @return int
     */
    protected function _getLikeCount() {
        $postService = new PostService();
        return $postService->getLikeCount($this);
    }

    /**
     * Get the virtual field hasLiked
     *
     * @return bool
     */
    protected function _getHasLiked() {
        $postService = new PostService();
        return $postService->hasLiked($this);
    }

    /**
     * Get the virtual field hasFlagged
     *
     * @return bool
     */
    protected function _getHasFlagged() {
        $postService = new PostService();
        return $postService->hasFlagged($this);
    }

    protected function _getIsStickied() {
        $postService = new PostService();
        return $postService->isStickied($this);
    }
}
