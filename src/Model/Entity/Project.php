<?php
namespace App\Model\Entity;

use App\Service\ProjectService;
use Cake\ORM\Entity;

/**
 * Project Entity.
 *
 * @property int $id
 * @property int $project_length
 * @property string $name
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Group[] $groups
 * @property \App\Model\Entity\Badge[] $badges
 * @property \App\Model\Entity\Operation[] $operations
 * @property \App\Model\Entity\Setting[] $settings
 * @property \App\Model\Entity\User[] $users
 */
class Project extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    public function getProjectCoordinators() {
        $projectService = new ProjectService();
        return $projectService->getProjectCoordinators($this->id);
    }
}
