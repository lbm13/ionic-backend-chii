<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FitbitActivity Entity.
 *
 * @property int $id
 * @property string $activity_name
 * @property int $average_heart_rate
 * @property int $fitbit_user_id
 * @property \App\Model\Entity\FitbitUser $fitbit_user
 * @property int $calories
 * @property float $distance
 * @property int $duration
 * @property \Cake\I18n\Time $last_modified
 * @property string $log_id
 * @property float $speed
 * @property \Cake\I18n\Time $start_time
 * @property int $steps
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $active_duration
 * @property string $heart_rate_link
 * @property string $log_type
 * @property string $tcx_link
 * @property \App\Model\Entity\FitbitHeartRateZone[] $fitbit_heart_rate_zones
 */
class FitbitActivity extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
