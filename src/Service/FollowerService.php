<?php
namespace App\Service;

use Cake\ORM\TableRegistry;

class FollowerService extends Service {
    public function follow($followerId, $followingId) {
        $object = $this->getModel()->findOrCreate([
            'user_id'      => $followerId,
            'following_id' => $followingId
        ]);

        return $object;
    }

    protected function getModel() {
        return TableRegistry::get('Followers');
    }

    public function unfollow($followerId, $followingId) {
        $followersTable = $this->getModel();
        $object = $followersTable->find('all')->where([
            'user_id'      => $followerId,
            'following_id' => $followingId
        ])->first();

        return $followersTable->delete($object);
    }
}
