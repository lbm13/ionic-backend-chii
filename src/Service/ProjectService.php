<?php
namespace App\Service;

use App\Controller\ProjectsController;
use App\Controller\UsersController;
use App\Model\Entity\User;
use App\Model\Entity\UserType;
use Cake\ORM\TableRegistry;

class ProjectService extends Service{
    public function findAllByUserId($userId) {
        $userProjects = TableRegistry::get('ProjectsUsers');
        $projectIds = $this->getField('project_id', $userProjects->findAllByUserId($userId)->toArray());

        if (count($projectIds) > 0) {
            $query = $this->getModel()->find('all', []);
            $query->where([
                "id IN" => $projectIds
            ]);
            return $query->toArray();
        } else {
            return [];
        }
    }

    protected function getModel() {
        return TableRegistry::get('Projects');
    }

    /**
     * @param integer $projectId
     *
     * @return User[]
     */
    public function getProjectCoordinators($projectId) {
        $usersModel = TableRegistry::get('Users');
        $query = $usersModel->find('all');
        $query = $query->innerJoinWith(
            'ProjectsUsers',
            function($q) use ($projectId) {
                return $q->where(['ProjectsUsers.project_id' => $projectId]);
            }
        );
        $query = $query->innerJoinWith(
            'UserTypes',
            function($q) {
                return $q->where(['UserTypes.name' => 'projectCoordinator']);
            }
        );

        return $query->toArray();
    }
}
