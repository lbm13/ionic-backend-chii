<?php
namespace App\Service;

use Cake\ORM\TableRegistry;

class MessageService extends Service  {
    public function getConversation($id = null, $page = null) {
        $parameters = ['limit' => '10'];
        if (!is_null($page)) {
            $parameters['offset'] = ($page - 1) * 10;
        }

        $query = TableRegistry::get('Conversations')->find('all', $parameters)->distinct();
        $query->leftJoinWith(
            'Messages.Recipients'
        );
        if (!is_null($id)) {
            $query->where(['Conversations.id' => $id]);
        }
        $query->where([
            'Messages.sender_id' => $GLOBALS['user']->id
        ]);
        $query->orWhere([
            'MessagesRecipients.recipient_id' => $GLOBALS['user']->id
        ]);
        $query->distinct();

        return $query->toArray();
    }

    public function getMessages($conversatonId = null) {
        $query = $this->getModel()->find('all');
        $query->contain(['Recipients', 'Senders', 'Senders.Images']);
        if (!is_null($conversatonId)) {
            $query->where(['conversation_id' => $conversatonId]);
        }
        $query->order('Messages.created');
        return $query->toArray();
    }

    protected function getModel() {
        return TableRegistry::get('Messages');
    }

    public function getResponseRecipients($conversationId) {
        $conversation = $this->getConversation($conversationId)[0];

        $recipients = [];
        $users = array_merge([$conversation->last_message->senders], $conversation->last_message->recipients);
        foreach ($users as $user) {
            if ($user->id != $GLOBALS['user']->id) {
                $recipients[] = $user;
            }
        }

        return $recipients;
    }

    public function updateReadStatus($messages) {
        $messageIds = $this->getIds($messages);

        TableRegistry::get('MessagesRecipients')->updateAll(
            [
                'has_read' => 1
            ],
            [
                'message_id IN' => $messageIds,
                'recipient_id'  => $GLOBALS['user']->id
            ]
        );
    }
}
