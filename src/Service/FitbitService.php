<?php
namespace App\Service;

use App\Model\Entity\FitbitActivity;
use App\Model\Entity\FitbitAuth;
use App\Model\Entity\FitbitUser;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * Main fitbit service that handles fitbit authorization and accessing of data through the fitbit api.
 *
 * Class FitBitService
 * @package App\Service
 */
class FitBitService {
    /**
     * Authorizes the user and gets a refresh token.
     *
     * @param int    $userId
     * @param int    $projectId
     * @param string $code
     * @param string $redirectUri
     *
     * @return FitbitUser|boolean
     */
    public function authorize($userId, $projectId, $code, $redirectUri) {
        $fitbitAuth = $this->getFitbitAuth($projectId);

        $fitbitResponse = $this->requestRefreshToken(
            [
                'grant_type'   => 'authorization_code',
                'client_id'    => urlencode($fitbitAuth->client_id),
                'code'         => urlencode($code),
                'redirect_uri' => $redirectUri
            ],
            $fitbitAuth
        );

        if (array_key_exists('user_id', $fitbitResponse) && array_key_exists('refresh_token', $fitbitResponse)) {
            return $this->updateFitbitUser($userId, $fitbitResponse['refresh_token'], $fitbitResponse['user_id']);
        } else {
            return false;
        }
    }

    private function createFitbitUser($userId, $fitbitUserId, $refreshToken) {
        $model = TableRegistry::get('FitbitUsers');
        $fitbitUser = $model->newEntity(
            [
                'fitbit_id'     => $fitbitUserId,
                'refresh_token' => $refreshToken,
                'user_id'       => $userId
            ]
        );

        return $model->save($fitbitUser);
    }

    private function fieldsToUrlParameters($fields) {
        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

        return $fields_string;
    }

    /**
     * Finds the user's FitbitUser
     *
     * @param int $userId
     *
     * @return FitbitUser|null
     */
    private function findFitbitUser($userId) {
        $query = TableRegistry::get('FitbitUsers')->find('all');
        $query = $query->where([
            'user_id' => $userId
        ]);
        return $query->first();
    }

    /**
     * Returns the project's FitbitAuth
     *
     * @param int $projectId
     *
     * @return FitbitAuth
     */
    private function getFitbitAuth($projectId) {
        return $this->getModel()->find('all')->where(['project_id' => $projectId])->first();
    }

    protected function getModel() {
        return TableRegistry::get('FitbitAuth');
    }

    /**
     * Request refresh token from fitbit
     *
     * @param mixed[]    $fields
     * @param FitbitAuth $fitbitAuth
     *
     * @return mixed
     */
    private function requestRefreshToken($fields, $fitbitAuth) {
        $header = base64_encode($fitbitAuth->client_id . ':' . $fitbitAuth->client_secret);
        $fieldCount = count($fields);
        $fields = $this->fieldsToUrlParameters($fields);

        $curl = curl_init();
        curl_setopt_array(
            $curl,
            [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST           => $fieldCount,
                CURLOPT_POSTFIELDS     => $fields,
                CURLOPT_URL            => 'https://api.fitbit.com/oauth2/token',
                CURLOPT_HTTPHEADER     => [
                    'AUTHORIZATION: Basic ' . $header
                ]
            ]
        );

        $response = curl_exec($curl);
        curl_close($curl);

        return json_decode($response, true);
    }

    /**
     * @param mixed[]    $activities
     * @param FitbitUser $fitbitUser
     *
     * @return FitbitActivity[]
     */
    private function saveActivities($activities, $fitbitUser) {
        $response = [];

        foreach($activities as $activity) {
            $activityObject = $this->saveActivity($activity, $fitbitUser);

            if (array_key_exists('heartRateZones', $activity)) {
                $activityObject->fitbit_heart_rate_zones = $this->saveHeartRateZones($activity, $activityObject);
            }

            $response[] = $activityObject;
        }

        return $response;
    }

    /**
     * @param mixed[]    $activity
     * @param FitbitUser $fitbitUser
     *
     * @return FitbitActivity
     */
    private function saveActivity($activity, $fitbitUser) {
        $activityModel = TableRegistry::get('FitbitActivities');

        $activityObject = $activityModel->find('all')->where(['log_id' => $activity['logId']])->first();
        if (!$activityObject) {
            /** @var FitbitActivity $activityObject */
            $activityObject = $activityModel->newEntity();
        }

        $activityObject->fitbit_user_id = $fitbitUser->id;
        foreach ($activity as $column => $value) {
            $column = Inflector::underscore($column);
            if ($activityModel->hasField($column)) {
                $activityObject[$column] = $value;
            }
        }

        return $activityModel->save($activityObject);
    }

    /**
     * @param $activity
     * @param $activityObject
     *
     * @return array
     */
    private function saveHeartRateZones($activity, $activityObject) {
        $heartRateZones = [];
        $heartRateZoneModel = TableRegistry::get('FitbitHeartRateZones');
        foreach ($activity['heartRateZones'] as $heartRateZone) {
            $heartRateZoneObject = $heartRateZoneModel->find('all')->where(
                [
                    'fitbit_activity_id' => $activityObject->id,
                    'name' => $heartRateZone['name']
                ]
            )->first();

            if (!$heartRateZoneObject) {
                $heartRateZoneObject = $heartRateZoneModel->newEntity();
                $heartRateZoneObject->fitbit_activity_id = $activityObject->id;
            }

            foreach ($heartRateZone as $column => $value) {
                $column = Inflector::underscore($column);
                if ($heartRateZoneModel->hasField($column)) {
                    $heartRateZoneObject[$column] = $value;
                }
            }

            $heartRateZoneObject = $heartRateZoneModel->save($heartRateZoneObject);
            if ($heartRateZoneObject) {
                $heartRateZones[] = $heartRateZoneObject;
            }
        }
        return $heartRateZones;
    }

    /**
     * Accesses fitbit activity data from the fitbit api.  Limit of 20 activities at a time, but has pagination support.
     * Will return all activities for a given fitbit user after the $afterDate.  Limit of 150 requests per hour!
     *
     * @param int         $projectId
     * @param int         $userId
     * @param string|null $afterDate
     *
     * @return FitbitActivity[]|boolean
     */
    public function updateActivities($projectId, $userId, $afterDate = null) {
        $fitbitAuth = $this->getFitbitAuth($projectId);
        $fitbitUser = $this->findFitbitUser($userId);

        $fitbitResponse = $this->requestRefreshToken(
            [
                'grant_type'    => 'refresh_token',
                'refresh_token' => urlencode($fitbitUser->refresh_token),
            ],
            $fitbitAuth
        );

        $fitbitUser = $this->updateFitbitUser($userId, $fitbitResponse['refresh_token']);
        if ($fitbitUser) {
            if (is_null($afterDate)) {
                $afterDate = '2000-01-01';
            }

            $url = 'https://api.fitbit.com/1/user/-/activities/list.json?';
            $url .= 'user-id='.$fitbitUser->fitbit_id;
            $url .= '&afterDate='.$afterDate;
            $url .= '&sort=desc&offset=0&limit=20';

            $activities = [];
            do {
                $curl = curl_init();
                curl_setopt_array(
                    $curl,
                    [
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_URL => $url,
                        CURLOPT_HTTPHEADER => [
                            'AUTHORIZATION: Bearer ' . $fitbitResponse['access_token']
                        ]
                    ]
                );

                $response = curl_exec($curl);
                $response = json_decode($response, true);

                if (is_array($response) && !array_key_exists('errors', $response)) {
                    foreach ($response['activities'] as $activity) {
                        array_push($activities, $activity);
                    }

                    $url = $response['pagination']['next'];
                } else {
                    $url = null;
                }
            } while ($url != '' || !is_null($url));

            curl_close($curl);

            return $this->saveActivities($activities, $fitbitUser);
        } else {
            return false;
        }
    }

    /**
     * Finds and updates the FitbitUser, if none exists it creates one
     *
     * @param int    $userId
     * @param string $refreshToken
     * @param string $fitbitUserId
     *
     * @return FitbitUser|boolean
     */
    private function updateFitbitUser($userId, $refreshToken, $fitbitUserId = null) {
        $model = TableRegistry::get('FitbitUsers');

        $fitbitUser = $this->findFitbitUser($userId);
        if (!is_null($fitbitUser)) {
            $fitbitUser->refresh_token = $refreshToken;
            $fitbitUser = $model->save($fitbitUser);
        } else if (!is_null($fitbitUserId)) {
            $fitbitUser = $this->createFitbitUser($userId, $fitbitUserId, $refreshToken);
        } else {
            return false;
        }

        return $fitbitUser;
    }
}
