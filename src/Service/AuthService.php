<?php
namespace App\Service;

use App\Controller\UsersController;
use App\Model\Entity\User;
use Cake\Auth\DefaultPasswordHasher;

class AuthService {
    /** @var UserService $userService */
    private $userService = null;

    public function __construct() {
        $this->userService = new UserService();
    }

    /**
     * Confirm the token is legit and return the associated user
     *
     * @param $token
     *
     * @return User|boolean
     */
    public function checkToken($token) {
        $user = $this->getUser($token);

        if (!is_null($user) && $this->compare($this->getTokenString($user), $this->stripUserId($token), false)) {
            return $user;
        } else {
            return false;
        }
    }

    /**
     * Compare two hashes
     *
     * @param $password
     * @param $hashedPassword
     * @param $decode
     *
     * @return bool
     */
    public function compare($password, $hashedPassword, $decode=true) {
        if ($decode) {
            $password = base64_decode($password);
        }

        $hasher = new DefaultPasswordHasher();
        return $hasher->check($password, $hashedPassword);
    }

    /**
     * Increment the user's login counter
     *
     * @param $token
     *
     * @return boolean
     */
    public function expireToken($token) {
        $userController = new UsersController();

        $user = $this->getUser($token);
        $user->set('logout_count', $user->logout_count + 1);
        $userController->Users->save($user);

        return $user;
    }

    /**
     * Generate a token for the user
     *
     * @param $user User
     *
     * @return string
     */
    public function generateToken($user) {
        $hasher = new DefaultPasswordHasher();

        $token = $user->id.'-'.$hasher->hash($this->getTokenString($user));
        $token = base64_encode($token);

        $userController = new UsersController();
        $user->loginCount = $user->logoutCount + 1;
        $userController->Users->save($user);

        return $token;
    }

    /**
     * Get the user associated with the token
     *
     * @param $token
     * @return User
     */
    public function getUser($token) {
        return $this->userService->getById($this->getUserId($token));
    }

    /**
     * Get the string for encoding the user
     *
     * @param $user
     *
     * @return string
     */
    public function getTokenString($user) {
        return $user->logout_count.$user->username.$user->password;
    }

    /**
     * Get the user id from the token
     *
     * @param $token
     * @return string
     */
    public function getUserId($token) {
        $token = base64_decode($token);
        return trim(substr($token, 0, strpos($token, '-')));
    }

    /**
     * Return the token without the prepended user id
     *
     * @param $token
     *
     * @return string
     */
    public function stripUserId($token) {
        $token = base64_decode($token);
        return substr($token, strpos($token, '-')+1);
    }
}
