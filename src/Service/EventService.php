<?php
namespace App\Service;

use App\Model\Entity\User;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

class EventService extends Service implements EventListenerInterface {

    public function generateSurveyEvent($event) {
        $this->generateEvent(
            $event->data->title,
            $event->data->description,
            $event->data->start,
            $event->data->end,
            $event->data->users
        );
    }

    public function generateUserEvent($event) {
        $this->generateEvent(
            $event->data->title,
            $event->data->description,
            $event->data->start,
            $event->data->end,
            $event->data->userId
        );
    }

    public function generateEvent($title, $description, $start, $end, $userId) {
        $eventModel = $this->getModel();

        if (is_array($userId)) {
            $event = [
                'title'       => $title,
                'description' => $description,
                'start'       => $start,
                'end'         => $end,
                'user_id'     => $GLOBALS['user']->id,
                'users'       => $userId
            ];
        } else {
            $event = [
                'title'       => $title,
                'description' => $description,
                'start'       => $start,
                'end'         => $end,
                'users' => [
                    ['id' => $userId]
                ]
            ];
        }

        $eventEntity = $eventModel->newEntity($event, ['associations' => 'EventsUsers']);
        return $eventModel->save($eventEntity);
    }

    protected function getModel() {
        return TableRegistry::get('events');
    }

    public function implementedEvents() {
        return [
            'survey.add.after' => 'generateSurveyEvent',
            'user.add.after' => 'generateUserEvent'
        ];
    }
}
