<?php
namespace App\Service;

use Cake\ORM\TableRegistry;

class VideoService extends Service {
    /**
     * Returns the number of likes on the video
     *
     * @param $video
     *
     * @return int
     */
    public function getLikeCount($video) {
        $videoLikeTable = TableRegistry::get("VideoLikes");
        $query = $videoLikeTable->find('all', []);
        $query->where(['video_id' => $video->id]);

        return $query->count();
    }

    protected function getModel() {
        return TableRegistry::get('Videos');
    }

    /**
     * Returns if the user has liked the video, if no user then it checks for the logged in user
     *
     * @param $video
     * @param null $user
     *
     * @return bool
     */
    public function hasLiked($video, $user=null) {
        if (is_null($user)) {
            $user = $GLOBALS['user'];
        }

        $videoLikeTable = TableRegistry::get("VideoLikes");
        $query = $videoLikeTable->find('all', []);
        $query->where([
            'user_id'  => $user->id,
            'video_id' => $video->id
        ]);

        return $query->count() > 0;
    }
}
