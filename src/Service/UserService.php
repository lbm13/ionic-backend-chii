<?php
namespace App\Service;

use App\Controller\UsersController;
use App\Model\Entity\Group;
use App\Model\Entity\Project;
use App\Model\Entity\User;
use App\Model\Entity\UserType;
use Cake\ORM\TableRegistry;

class UserService extends Service {
    /**
     * Get user by id
     *
     * @param $id
     *
     * @return User|null
     */
    public function getById($id) {
        $userController = new UsersController();
        $query = $userController->Users->find('all', []);
        $query->contain(['Images', 'Groups', 'Projects', 'UserTypes']);
        $query->where(
            ["Users.id" => $id]
        );

        $user = $query->last();
        return ($user) ? $user : null;
    }

    /**
     * Return the associated group, returns null if no group_id is set
     *
     * @param User $user
     *
     * @return null|Group
     */
    public function getGroup($user) {
        if (is_null($user->group_id)) {
            return null;
        } else {
            return TableRegistry::get('Groups')->find('all')->where(['id' => $user->group_id])->first();
        }
    }

    protected function getModel() {
        return TableRegistry::get('Users');
    }

    /**
     * Get the user's userType
     *
     * @param User $user
     *
     * @return UserType
     */
    public function getUserType(User $user) {
        $userTypeService = new UserTypeService();
        return $userTypeService->getById($user->user_type_id);
    }

    /**
     * Get an array of the user's projects
     *
     * @param User $user
     *
     * @return Project
     */
    public function getProjects(User $user) {
        $projectService = new ProjectService();
        return $projectService->findAllByUserId($user->id);
    }

    public function isFollower(User $user, $activeUser = null) {
        if (is_null($activeUser)) {
            if (array_key_exists('user', $GLOBALS)) {
                $activeUser = $GLOBALS['user'];
            } else {
                return null;
            }
        }

        if ($activeUser->id === $user->id) {
            return null;
        }

        $followerTable = TableRegistry::get('Followers');
        $query = $followerTable->find('all', []);
        $query = $query->where([
            'following_id' => $activeUser->id,
            'user_id'      => $user->id
        ]);

        return count($query->toArray()) > 0;
    }

    public function isFollowing(User $user, $activeUser = null) {
        if (is_null($activeUser)) {
            if (array_key_exists('user', $GLOBALS)) {
                $activeUser = $GLOBALS['user'];
            } else {
                return null;
            }
        }

        if ($activeUser->id === $user->id) {
            return null;
        }

        $followerTable = TableRegistry::get('Followers');
        $query = $followerTable->find('all', []);
        $query = $query->where([
            'following_id' => $user->id,
            'user_id'      => $activeUser->id
        ]);

        return count($query->toArray()) > 0;
    }
}
