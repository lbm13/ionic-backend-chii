<?php
namespace App\Service;

use App\Controller\AnswerInputTypesController;
use App\Model\Entity\AnswerInputType;
use Cake\ORM\TableRegistry;

class AnswerInputTypeService extends Service {
    protected function getModel() {
        return TableRegistry::get('AnswerInputTypes');
    }
}
