<?php
namespace App\Service;

use App\Model\Entity\User;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

class MonitorService extends Service implements EventListenerInterface {
    /**
     * @param Event $event
     */
    public function checkBlacklistedWords($event) {
        /** @var User $user */
        $user = $GLOBALS['user'];
        $projectCoordinatorsIds = $this->getField('id', $user->getProjects()[0]->getProjectCoordinators());

        $blacklistedWordService = new BlacklistedWordService();
        if ($blacklistedWordService->containsBlacklistedWord($event->data->message)) {
            $notificationService = new NotificationService();
            $notificationService->generateNotification(
                'monitor',
                $projectCoordinatorsIds,
                'A user created content using a blacklisted word.'
            );
        }
    }

    /**
     * Returns a list of events this object is implementing. When the class is registered
     * in an event manager, each individual method will be associated with the respective event.
     *
     * @return array
     */
    public function implementedEvents() {
        return [
            'post.add.after' => 'checkBlacklistedWords'
        ];
    }

    protected function getModel() {
        return null;
    }
}
