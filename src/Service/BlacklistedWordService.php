<?php
namespace App\Service;

use Cake\ORM\TableRegistry;

class BlacklistedWordService extends Service {
    /**
     * Check if text contains a blacklisted word
     *
     * @param string $text
     *
     * @return bool
     */
    public function containsBlacklistedWord($text) {
        $words = $this->getField('word', $this->getModel()->find('all')->toArray());
        foreach ($words as $word) {
            if (strpos(strtolower($text), strtolower($word)) !== false) {
                return true;
            }
        }

        return false;
    }

    protected function getModel() {
        return TableRegistry::get('BlacklistedWords');
    }
}
