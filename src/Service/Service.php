<?php
namespace App\Service;

use Cake\ORM\Entity;
use Cake\ORM\Table;

abstract class Service {
    /**
     * @return Table
     */
    protected abstract function getModel();

    public function getById($id) {
        return $this->getModel()->find('all')->where(['id' => $id])->first();
    }

    public function getId(Entity $entity) {
        return $entity->id;
    }

    public function getIds($entities) {
        return $this->getField('id', $entities);
    }

    public function getField($field, $entities) {
        if (!is_array($entities)) {
            $entities = [$entities];
        }

        $fields = [];
        foreach ($entities as $entity) {
            $fields[] = $entity->$field;
        }

        return $fields;
    }

    /**
     * Convert the array of entities into an array with keys matching the entity ids
     *
     * @param $entities
     * @param string $property
     *
     * @return array
     */
    public function toArrayById($entities, $property='id') {
        if (!is_array($entities)) {
            $entities = [$entities];
        }

        $array = [];
        foreach ($entities as $entity) {
            $array[$entity[$property]] = $entity;
        }

        return $array;
    }
}
