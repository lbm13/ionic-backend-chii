<?php
namespace App\Service;

use App\Model\Entity\AnswerInputType;
use Cake\ORM\TableRegistry;

class QuestionService extends Service {
    /**
     * Get the AnswerInputType from the question
     *
     * @param $question
     *
     * @return AnswerInputType
     */
    public function getAnswerInputType($question) {
        $answerInputTypeService = new AnswerInputTypeService();
        return $answerInputTypeService->getById($question->answer_input_type_id);
    }

    protected function getModel() {
        return TableRegistry::get('Questions');
    }
}
