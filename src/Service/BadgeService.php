<?php
namespace App\Service;

use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

class BadgeService extends Service {

    public function getBadges($userId, $selectedProjectId){
        $badges = $this->getModel()
            ->find('all')
            ->contain(['Images', 'Users' => function (Query $q) use ($userId) {
                return $q->where(['Users.id' => $userId]);
            }])
            ->matching('Projects', function (Query $q) use ($selectedProjectId) {
                return $q->where(['Projects.id' => $selectedProjectId]);
            })
            ->toArray();
        if (!is_null($userId)) {
            $badges = $this->getBadgeCompletions($badges, $userId);
        }
        return $badges;
    }

    public function getBadgeCompletions($badges, $userId) {
        foreach ($badges as $badge) {
            $percentage = 0;
            if (count($badge['users']) > 0) {
                $percentage = 100;
            } else {
                $badgesProjects = $badge['_matchingData']['BadgesProjects'];
                if (!is_null($badgesProjects['reward_rule_key'])) {
                    $reward_rule_key = $badgesProjects['reward_rule_key'];
                    if (!is_null($badgesProjects['reward_rule_key'])) {
                        $reward_rule_value = $badgesProjects['reward_rule_value'];
                        switch($reward_rule_key) {
                            case 'steps':
                                $percentage = $this->steps($userId, $reward_rule_value);
                                break;
                        }
                    }
                }
            }
            $badge['percentageCompleted'] = $percentage;
        }
        return $badges;
    }

    public function steps($userId, $reward_rule_value) {
        $fitbitModel = TableRegistry::get('FitbitActivities')->find('all');
        $steps = $fitbitModel
            ->select([
                'steps',
                'count' => $fitbitModel->func()->count('*')
            ])
            ->matching('FitbitUsers', function(Query $q) use ($userId) {
                return $q->where(['FitbitUsers.user_id' => $userId]);
            })
            ->first();
        return ($steps['steps'] / $reward_rule_value) * 100;
    }

    protected function getModel() {
        return TableRegistry::get('Badges');
    }
}
