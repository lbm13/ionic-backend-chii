<?php
namespace App\Service;

use App\Model\Entity\User;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

class NotificationService extends Service implements EventListenerInterface {
    public function generateEventNotification($event) {
        if ($event->data->user_id != $event->data->creator_id) {
            $this->generateNotification(
                'app.calendar',
                $event->data->user_id,
                'An event has been added to your calendar.'
            );
        }
    }

    public function generateSurveyNotification($event) {
        $userIds = $event->data->users;
        $notificationModel = $this->getModel();
        for ($i = 0; $i < count($userIds); $i++) {
            $notification = [
                'message' => 'An event has been added to your calendar.',
                'type'    => 'app.calendar',
                'user_id' => $userIds[$i]['id']
            ];

            $theNotification = $notificationModel->newEntity($notification);
            $notificationModel->save($theNotification);
        }
    }

    public function generateFriendAddNotification($event) {
        $this->generateNotification(
            'app.friends',
            $event->data->following_id,
            'You have a new friend.'
        );
    }

    public function generateFriendRequestNotification($event) {
        $this->generateNotification(
            'app.friends',
            $event->data->following_id,
            'You have a new friend\'s request.'
        );
    }

    /**
     * @param Event $event
     */
    public function generateMonitorNotification($event) {
        /** @var User $user */
        $user = $GLOBALS['user'];
        $projectCoordinatorsIds = $this->getField('id', $user->getProjects()[0]->getProjectCoordinators());

        $this->generateNotification(
            'app.monitoring',
            $projectCoordinatorsIds,
            'A user has reported content.'
        );
    }

    /**
     * @param Event $event
     */
    public function generatePostNotification($event) {
        $postId = $event->data->id;
        $type = "app.post({postId: $postId})";
        $this->generateNotification(
            $type,
            $event->data->to_user_id,
            'You have a new post.'
        );
    }

    public function generateNotification($type, $userIds, $message) {
        $notificationModel = $this->getModel();

        if (!is_array($userIds)) {
            $userIds = [$userIds];
        }

        $notifications = [];
        foreach ($userIds as $userId) {
            $notification = [
                'message' => $message,
                'type'    => $type,
                'user_id' => $userId
            ];

            $notifications[] = $notificationModel->newEntity($notification);
        }

        return $notificationModel->saveMany($notifications);
    }

    protected function getModel() {
        return TableRegistry::get('Notifications');
    }

    /**
     * Returns a list of events this object is implementing. When the class is registered
     * in an event manager, each individual method will be associated with the respective event.
     *
     * @return array
     */
    public function implementedEvents() {
        return [
            'event.add.after'      => 'generateEventNotification',
            'friend.add.after'     => 'generateFriendAddNotification',
            'friend.request.after' => 'generateFriendRequestNotification',
            'monitor.add.comments' => 'generateMonitorNotification',
            'monitor.add.posts'    => 'generateMonitorNotification',
            'post.add.after'       => 'generatePostNotification',
            'survey.add.notify'    => 'generateSurveyNotification'
        ];
    }
}
