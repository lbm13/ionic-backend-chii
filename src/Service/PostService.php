<?php
namespace App\Service;

use Cake\ORM\TableRegistry;

class PostService extends Service {
    /**
     * Returns the number of likes on the post
     *
     * @param $post
     *
     * @return int
     */
    public function getLikeCount($post) {
        $postLikesTable = TableRegistry::get("PostLikes");
        $query = $postLikesTable->find('all', []);
        $query->where(['post_id' => $post->id]);

        return $query->count();
    }

    protected function getModel() {
        return TableRegistry::get('Posts');
    }

    /**
     * Returns if the user has liked the post, if no user then it checks for the logged in user
     *
     * @param $post
     * @param null $user
     *
     * @return bool
     */
    public function hasLiked($post, $user=null) {
        if (is_null($user)) {
            $user = $GLOBALS['user'];
        }

        $postLikesTable = TableRegistry::get("PostLikes");
        $query = $postLikesTable->find('all', []);
        $query->where([
            'user_id'  => $user->id,
            'post_id' => $post->id
        ]);

        return $query->count() > 0;
    }

    public function hasFlagged($post) {
        return !is_null(TableRegistry::get('ReportedPosts')->find('all')->where(
            [
                'post_id' => $post->id,
                'user_id' => $GLOBALS['user']->id
            ]
        )->first());
    }

    public function isStickied($post) {
        if (is_null($post->stickied)) {
            return false;
        }

        return date('Y-m-d', strtotime($post->stickied)) > date('Y-m-d');
    }
}
