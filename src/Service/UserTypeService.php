<?php
namespace App\Service;

use App\Controller\UserTypesController;
use App\Model\Entity\User;
use App\Model\Entity\UserType;
use Cake\ORM\TableRegistry;

class UserTypeService extends Service {
    protected function getModel() {
        return TableRegistry::get('UserTypes');
    }
}