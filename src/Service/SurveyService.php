<?php
namespace App\Service;

use App\Controller\SurveysController;
use App\Model\Entity\Answer;
use App\Model\Entity\Survey;
use App\Model\Entity\SurveysUser;
use Cake\ORM\TableRegistry;

class SurveyService extends Service {
    /**
     * @param Survey       $survey
     * @param integer|null $userId
     * @param integer|null $responseOrder
     *
     * @return Answer[]
     */
    public function getAnswers($survey, $userId=null, $responseOrder=null) {
        $contains = [];
        if (is_null($userId)) {
            $contains['user_id'] = $userId;
        }
        if (!is_null($responseOrder)) {
            $contains['response_order'] = $responseOrder;
        }

        $surveyController = new SurveysController();
        $query = $surveyController->Surveys->find('all');
        foreach ($contains as $key => $value) {
            $query->contain([
                'Questions.Answers' => function($q) use ($key, $value) {
                    return $q->where([$key => $value]);
                }
            ]);
        }
        $query->where([
            'id' => $survey->id
        ])->first();

        $answers = [];
        $questions = $query->toArray()[0]->questions;
        foreach ($questions as $question) {
            $answers = array_merge($answers, $question->answers);
        }

        return $answers;
    }

    protected function getModel() {
        return TableRegistry::get('Surveys');
    }

    public function getResponseCount($id, $userId) {
        $query = TableRegistry::get('SurveysUsers')->find();
        $query->select(['maxResponse' => $query->func()->max('response_order')]);
        $query->where([
            'response_order >' => 0,
            'survey_id' => $id,
            'user_id'   => $userId
        ]);

        return ($query->count() == 0) ? 1 : intval($query->first()->maxResponse) + 1;
    }

    public function hasCompleted($survey, $user) {
        $surveyId = $survey->id;
        $userId = $user->id;

        $query = TableRegistry::get('Questions')->find('all');
        $query = $query->where(['survey_id' => $surveyId]);
        $questionCount = $query->count();

        $query = TableRegistry::get('answers')->find('all');
        $query = $query->innerJoinWith(
            'SurveysUsers',
            function($q) use ($surveyId, $userId) {
                return $q->where(
                    [
                        'response_order' => 0,
                        'survey_id'      => $surveyId,
                        'user_id'        => $userId
                    ]
                );
            }
        );
        $answersCount = $query->count();

        return $questionCount == $answersCount;
    }

    public function resend($surveyId, $userId) {
        $surveyUserModel = TableRegistry::get('SurveysUsers');
        $query = $surveyUserModel->query()->find('all');
        $query = $query->where([
            'survey_id' => $surveyId,
            'user_id'   => $userId
        ]);

        $responseOrder = 0;
        /** @var SurveysUser[] $results */
        $results = $query->toArray();
        foreach ($results as $result) {
            if ($result->response_order > $responseOrder) {
                $responseOrder = $result->response_order;
            }
        }

        $surveyUserModel->updateAll(
            [
                'response_order' => $responseOrder + 1
            ],
            [
                'is_locked'      => 1,
                'response_order' => 0,
                'survey_id'      => $surveyId,
                'user_id'        => $userId
            ]
        );
    }

    /**
     * Lock/Unlock surveys_users
     *
     * @param integer $surveyId
     * @param integer $userId
     * @param boolean $value
     */
    public function toggleLock($surveyId, $userId, $value) {
        $surveyUserModel = TableRegistry::get('SurveysUsers');
        $surveyUserModel->updateAll(
            [
                'is_locked' => $value
            ],
            [
                'response_order' => 0,
                'survey_id'      => $surveyId,
                'user_id'        => $userId
            ]
        );
    }
}
