<?php
namespace App\Service;

use App\Controller\PostsController;
use App\Model\Entity\Post;

class NewsFeedService {
    /**
     * @param $posts Post[]
     * @return string[]
     */
    private function formatPosts($posts) {
        if (!is_array($posts)) {
            $posts = [$posts];
        }

        $formattedPosts = [];
        foreach ($posts as $post) {
            $formattedPosts[] = [
                'from' => [
                    'id'        => $post->fromUserId,
                    'firstName' => $post->fromUser->firstName,
                    'lastName'  => $post->fromUser->lastName,
                    'username'  => $post->fromUser->username
                ],
                'to' => [
                    'id'        => $post->toUserId,
                    'firstName' => $post->toUser->firstName,
                    'lastName'  => $post->toUser->lastName,
                    'username'  => $post->toUser->username
                ],
                'content' => [
                    'content'   => $post->message,
                    'createdAt' => $post->createdAt,
                    'type'      => 'text'
                ]
            ];
        }

        return $formattedPosts;
    }

    public function generate() {
        $postsController = new PostsController();
        $postsQuery = $postsController->Posts->find('all');
        $postsQuery->contain(['fromUser', 'toUser']);
        $postsQuery->orWhere([
            "fromUser.projectId" => $GLOBALS['user']->projectId,
            "toUser.projectId" => $GLOBALS['user']->projectId
        ]);
        $postsQuery->orderDesc('createdAt');
        $postsQuery->limit(3);
        return $this->formatPosts($postsQuery->toArray());
    }
}