<?php
namespace App\Service;

use App\Controller\UsersController;

class GraphService {
    /**
     * Generates data for a discrete bar graph using the login count of users from the same project
     *
     * @return \stdClass
     */
    public function getLoginData() {
        $projectService = new ProjectService();
        $projectIds = $projectService->getIds($GLOBALS['user']->getProjects());

        $userController = new UsersController();
        $userQuery = $userController->Users->find();
        $userQuery->where([
            "projectId IN" => $projectIds
        ]);
        $userQuery->orderDesc("username");

        $data = new \stdClass();
        $data->format = ',';
        $data->showValues = true;

        $data->title = new \stdClass();
        $data->title->enable = true;
        $data->title->text = 'Users\' Login Count';

        $data->xAxis = new \stdClass();
        $data->xAxis->axisLabel = 'User';

        $data->yAxis = new \stdClass();
        $data->yAxis->axisLabel = 'Login Count';

        $data->values = [];
        foreach ($userQuery->toArray() as $user) {
            $value = new \stdClass();
            $value->label = $user->username;
            $value->value = $user->loginCount;

            $data->values[] = $value;
        }

        return $data;
    }
}