<?php
namespace App\Network;

use App\Network\HttpResponseCode;
use Cake\Datasource\EntityInterface;
use Cake\Network\Response;

class UABResponse extends Response  {
    private $statusCode = HttpResponseCode::RESPONSE_OK;
    private $messages = [];

    /**
     * Add an error to the messages
     *
     * @param string $error
     * @param int    $statusCode
     */
    public function addError($error, $statusCode=HttpResponseCode::RESPONSE_INVALID_FIELD) {
        if (!array_key_exists('errors', $this->messages)) {
            $this->messages['errors'] = [];
        }

        $this->messages['errors'][] = $error;
        $this->statusCode = $statusCode;
    }

    /**
     * Add an array of errors
     *
     * @param $errors
     * @param int $statusCode
     */
    public function addErrors($errors, $statusCode=HttpResponseCode::RESPONSE_INVALID_FIELD) {
        if (!is_array($errors)) {
            $errors = [$errors];
        }

        foreach ($errors as $error) {
            $this->addError($error, $statusCode);
        }
    }

    /**
     * Add a message to the Response, merge on keys if necessary
     *
     * @param string                 $key
     * @param EntityInterface|string $value
     * @param null|HttpResponseCode  $statusCode
     */
    public function addMessage($key, $value, $statusCode=null) {
        if (array_key_exists($key, $this->messages)) {
            $this->mergeMessages($key, $value);
        } else {
            $this->messages[$key] = $value;
        }

        if (!is_null($statusCode)) {
            $this->statusCode = $statusCode;
        }
    }

    /**
     * Remove all messages from response
     *
     * @return void
     */
    public function clearMessages() {
        $this->messages = [];
    }

    /**
     * Send response immediately and die
     */
    public function kill() {
        $this->send();
        die();
    }

    /**
     * Remove message by key
     *
     * @param string $key
     *
     * @return bool
     */
    public function removeMessage($key) {
        if (array_key_exists($key, $this->messages)) {
            unset($this->messages[$key]);

            return true;
        }

        return false;
    }

    /**
     * Send response
     */
    public function send() {
        $this->type('json');
        $this->statusCode($this->statusCode);
        $this->body(json_encode($this->messages));

        parent::send();
    }

    /**
     * Merge an external Response into this object,
     * precedent is given to the response being merged
     *
     * @param UABResponse $response
     */
    public function merge(UABResponse $response) {
        $this->statusCode = $response->statusCode;

        foreach ($response->messages as $key => $value) {
            if ($this->has($key)) {
                $this->mergeMessages($key, $value);
            } else {
                $this->messages[$key] = $value;
            }
        }
    }

    /**
     * Merge another Response's messages based on key
     *
     * @param string $key
     * @param array  $messages
     */
    public function mergeMessages($key, $messages) {
        if (!is_array($this->messages[$key])) {
            $this->messages[$key] = [$this->messages[$key]];
        }

        if (!is_array($messages)) {
            $messages = [$messages];
        }

        foreach ($messages as $message) {
            $this->messages[$key][] = $message;
        }
    }

    /**
     * Check if the Response message exists
     *
     * @param string $key
     *
     * @return bool
     */
    private function has($key) {
        return array_key_exists($key, $this->messages);
    }

    /**
     * Check if the Response has any errors
     *
     * @return bool
     */
    public function hasErrors() {
        return $this->has('errors');
    }
}
