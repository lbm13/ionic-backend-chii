<?php
namespace App\Network;

class HttpResponseCode {
    const RESPONSE_OK = 200;
    const RESPONSE_UPDATED = 200;
    const RESPONSE_CREATED = 201;
    const RESPONSE_DELETED = 204;
    const RESPONSE_NOT_CHANGED = 304;
    const RESPONSE_INVALID_FIELD = 400;
    const RESPONSE_ACCESS_DENIED = 401;
    const RESPONSE_NOT_FOUND = 404;
    const RESPONSE_NOT_ALLOWED = 405;
    const RESPONSE_ILLEGAL_OPERATION = 409;
    const RESPONSE_INTERNAL_SERVER_ERROR = 500;
}
