<?php

namespace App\Shell;

use App\Model\Entity\FitbitActivity;
use App\Model\Entity\FitbitUser;
use App\Model\Entity\ProjectsUser;
use Cake\Console\Shell;
use App\Service\FitbitService;
use Cake\ORM\TableRegistry;

/**
 * Fitbit CRON class.
 *
 * Class FitBitShell
 * @package App\Shell
 */
class FitBitShell extends Shell {
    public function main() {
        $fitbitService = new FitbitService();

        $fitbitActivitiesModel = TableRegistry::get('FitbitActivities');
        $fitbitUsersModel = TableRegistry::get('FitbitUsers');
        $projectsUsersModel = TableRegistry::get('ProjectsUsers');

        /** @var FitbitUser[] $fitbitUsers */
        $fitbitUsers = $fitbitUsersModel->find('all');
        foreach ($fitbitUsers as $fitbitUser) {
            /** @var ProjectsUser[] $projects */
            $projects = $projectsUsersModel->find('all')->where(['user_id' => $fitbitUser->user_id])->toArray();
            foreach ($projects as $project) {
                /** @var FitbitActivity $fitbitActivity */
                $fitbitActivity = $fitbitActivitiesModel->find('all')->where(
                    [
                        'fitbit_user_id' => $fitbitUser->id
                    ]
                )->orderDesc('start_time')->first();

                $fitbitService->updateActivities($project->id, $fitbitUser->user_id, $fitbitActivity->start_time);
            }
        }
    }
}
