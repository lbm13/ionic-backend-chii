<?php
namespace Cake\Error;

class UabExceptionRenderer extends ExceptionRenderer {
    /**
     * Renders the response for the exception.
     */
    public function render() {
        $message = $this->error->getMessage().'; line '.$this->error->getLine();
        $statusCode = $this->error->getCode();

        $this->controller->response->addError($message, $statusCode);
        $this->controller->response->kill();
    }
}
